package tyre.commandline;

/**
 * Abstract class that provides the necessary functionality for polymorphic command handling.
 */
public abstract class AbstractCommand {
    /**
     * Parameters passed to the command.
     * They should NOT include the name of the command itself.
     */
    protected String[] params;

    /**
     * Runs the command by invoking {@link #process()}.
     *
     * @param params Passed parameters.
     */
    public void run(String[] params) {
        this.params = params;

        process();
    }

    /**
     * Method to be overridden for processing of commands
     */
    protected abstract void process();

    /**
     * Method returning name of command.
     *
     * @return Name of command that will be used when user invokes said command.
     */
    public abstract String getCommandName();

    /**
     * Gets the output returned by the command after {@link #run(String[])}
     *
     * @return Output of the command.
     */
    public abstract String getOutput();
}
