package tyre.commandline;

import tyre.commons.requirements.management.RequirementManager;

/**
 * Command that lists all requirements.
 */
public class ListRequirementsCommand extends AbstractCommand {
    private String output;
    private CacheFindRequirementsCommand find;

    public ListRequirementsCommand(RequirementManager manager) {
        this.find = new CacheFindRequirementsCommand(manager);
    }

    @Override
    protected void process() {
        String[] params = new String[0];
        this.find.run(params);
        this.output = this.find.getOutput();
    }

    @Override
    public String getCommandName() {
        return "list";
    }

    @Override
    public String getOutput() {
        return this.output;
    }
}
