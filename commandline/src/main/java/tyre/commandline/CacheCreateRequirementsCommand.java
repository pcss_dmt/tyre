package tyre.commandline;

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;
import tyre.commons.externalData.provider.CommandlineProvider;
import tyre.commons.parser.FileParser;
import tyre.commons.requirements.management.RequirementManager;
import tyre.commons.requirements.model.Requirement;

/**
 * Command used to create a new requirement from given arguments
 */

//@req(REQ-71)
public class CacheCreateRequirementsCommand extends AbstractCommand {

    private final RequirementManager manager;
    private CommandlineProvider provider;
    private String message = "";
    private String errorMessage = "You need an argument for option %s%n";

    public CacheCreateRequirementsCommand(CommandlineProvider provider, RequirementManager manager) {
        this.provider = provider;
        this.manager = manager;
    }

    @Override
    protected void process() {

        LongOpt[] longOpts = getConsoleInputConfiguration();
        Getopt input = new Getopt("args", this.params, "", longOpts);

        Requirement req = createRequirementFromConsoleInput(input);

        if (req != null && this.provider.addRequirement(req)) {
            this.message = String.format("\t%s %s %s %s%n", req.getId(), req.getTitle(), req.getVersion(), req.getLocation());

            // synchronize changes
            manager.getAllRequirements();
        }
    }

    /**
     * Creates array with the definitions of required console parameters.
     *
     * @return - array of parameters definitions
     */
    private LongOpt[] getConsoleInputConfiguration() {
        LongOpt[] longOpts = new LongOpt[4];
        longOpts[0] = new LongOpt("id", LongOpt.REQUIRED_ARGUMENT, null, 'i');
        longOpts[1] = new LongOpt("title", LongOpt.REQUIRED_ARGUMENT, null, 't');
        longOpts[2] = new LongOpt("version", LongOpt.REQUIRED_ARGUMENT, null, 'v');
        longOpts[3] = new LongOpt("location", LongOpt.REQUIRED_ARGUMENT, null, 'l');
        return longOpts;
    }

    /**
     * Creates requirement if all parameters are passed from input
     *
     * @param input - console input.
     * @return -returns created requirement or null if error occurs.
     */
    private Requirement createRequirementFromConsoleInput(Getopt input) {
        String id = null;
        String title = null;
        String location = null;
        String version = FileParser.NO_VERSION;
        int c;
        while ((c = input.getopt()) != -1) {
            switch (c) {
                case 'i':
                    id = input.getOptarg();
                    break;
                case 't':
                    title = input.getOptarg();
                    break;
                case 'v':
                    version = input.getOptarg();
                    break;
                case 'l':
                    location = input.getOptarg();
                    break;
                case ':':
                    this.message = String.format(errorMessage, (char) c);
                    return null;
                case '?':
                    this.message = "This option is not valid\n";
                    return null;
                default:
                    break;
            }
        }

        if (isIdMissing(id)) {
            return null;
        }

        return new Requirement(id, version, title, location);
    }

    /**
     * Checks if given parameters are passed
     *
     * @param id Requirement Id
     * @return True if any of demanded parameters is missing
     */
    private boolean isIdMissing(String id) {
        boolean isMissing = false;
        if (id == null) {
            this.message = String.format(errorMessage, "id");
            isMissing = true;
        }
        return isMissing;
    }

    @Override
    public String getOutput() {
        return this.message;
    }

    //@req(REQ-138)
    @Override
    public String getCommandName() {
        return "cache-create";
    }

}
