package tyre.commandline;

import tyre.commons.configuration.ConfigurationManager;

/**
 * Command that outputs the version message.
 */
public class VersionCommand extends AbstractCommand {
    @Override
    protected void process() {
        // No need to process anything.
    }

    @Override
    public String getCommandName() {
        return "version";
    }

    @Override
    public String getOutput() {
        return "TYRE Requirement Tracker version " + ConfigurationManager.getInstance().getVersion() + "\n";
    }
}
