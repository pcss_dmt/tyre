package tyre.commandline;

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;
import tyre.commons.configuration.ConfigurationManager;
import tyre.commons.parser.FileParser;
import tyre.commons.requirements.management.RequirementManager;
import tyre.commons.requirements.model.Requirement;
import tyre.commons.utils.filtering.Visitor;
import tyre.commons.utils.filtering.requirement.RequirementFinder;

import java.util.List;

/**
 * Command class responsible for listing, filtering and sorting of requirements.
 */
//@req(REQ-70)
public class CacheFindRequirementsCommand extends AbstractCommand {


    /**
     * Used requirement provider.
     */
    private RequirementManager requirementManager;

    /**
     * List of filtered/sorted requirements.
     */
    private List<Requirement> list;

    /**
     * Output string.
     */
    private String output;

    /**
     * Filtering/sorting object.
     */
    private RequirementFinder requirementFilter;

    public CacheFindRequirementsCommand(RequirementManager requirementManager) {
        this.requirementManager = requirementManager;
    }

    /**
     * Static method that returns formatted output for requirement data.
     * @param requirement Requirement to be formatted.
     * @return Requirement data formatted.
     */
    //@req(REQ-141)
    public static String getRequirementInfo(Requirement requirement) {
        StringBuilder reqInfoBuilder = new StringBuilder();
        reqInfoBuilder.append(String.format("ID: %s%n", requirement.getId()));
        reqInfoBuilder.append(String.format("Version: %s%n",
                requirement.getVersion() == null || requirement.getVersion().equals(FileParser.NO_VERSION)
                        ? "<no version>" : requirement.getVersion()));
        if (requirement.getTitle() != null && !requirement.getTitle().isEmpty())
            reqInfoBuilder.append(String.format("Title: %s%n", requirement.getTitle()));
        if (requirement.getLocation() != null && !requirement.getLocation().isEmpty())
            reqInfoBuilder.append(String.format("Documents:%n\t%s%n", requirement.getLocation()));
        reqInfoBuilder.append(String.format("%n"));

        return reqInfoBuilder.toString();
    }

    /**
     * Gets visitor that is tied with argument provided with sorting flag. If argument does not match any of field names
     * it sorts by id by default.
     * @param argument argument provided with sorting flag
     * @return visitor that is matched with provided argument.
     */
    private Visitor<Requirement, String> getVisitor(String argument) {
        if ("version".equals(argument)) {
            return RequirementFinder.versionVisitor;
        } else if ("id".equals(argument)) {
            return RequirementFinder.idVisitor;
        } else if ("location".equals(argument)) {
            return RequirementFinder.locationVisitor;
        } else if ("title".equals(argument)) {
            return RequirementFinder.titleVisitor;
        }
        return RequirementFinder.idVisitor;           /* sort by id by default */
    }

    /**
     * Handle arguments that were passed to the command.
     * @param getopt Getopt object that contains the array of parameters.
     */
    private void handleArguments(Getopt getopt) {
        int c;

        while ((c = getopt.getopt()) != -1) {
            switch (c) {
                // filtering below:
                case 'i':
                    requirementFilter.addFilter(getopt.getOptarg(), RequirementFinder.idVisitor);
                    break;

                case 't':
                    requirementFilter.addFilter(getopt.getOptarg(), RequirementFinder.titleVisitor);
                    break;

                case 'v':
                    requirementFilter.addFilter(getopt.getOptarg(), RequirementFinder.versionVisitor);
                    break;

                case 'l':
                    requirementFilter.addFilter(getopt.getOptarg(), RequirementFinder.locationVisitor);
                    break;

                // sorting below:
                // sort ascending
                case 'a':
                    requirementFilter.setSortAscending(getVisitor(getopt.getOptarg()));
                    break;

                // sort descending
                case 'd':
                    requirementFilter.setSortDescending(getVisitor(getopt.getOptarg()));
                    break;

                default:
                    break;
            }
        }

        // when there is a non-flag option at the end, we will filter by id
        if(getopt.getOptind() < this.params.length) {
            String[] splitParameter = this.params[getopt.getOptind()].split(":");

            requirementFilter.addFilter(splitParameter[0], RequirementFinder.idVisitor);
            if(splitParameter.length == 2) {
                requirementFilter.addFilter(splitParameter[1], RequirementFinder.versionVisitor);
            }
        }
    }

    private void initialize() {
        this.output = "";
        this.list = null;
        this.requirementFilter = new RequirementFinder();
    }

    /**
     * Processes the arguments and then filters, sorts or lists the requirements accordingly.
     */
    @Override
    protected void process() {
        this.initialize();
        if (this.params.length > 0) {
            // handle arguments
            LongOpt[] longOpts = {
                    new LongOpt("id", LongOpt.REQUIRED_ARGUMENT, null, 'i'),
                    new LongOpt("title", LongOpt.REQUIRED_ARGUMENT, null, 't'),
                    new LongOpt("version", LongOpt.REQUIRED_ARGUMENT, null, 'v'),
                    new LongOpt("location", LongOpt.REQUIRED_ARGUMENT, null, 'l'),
                    new LongOpt("asc", LongOpt.REQUIRED_ARGUMENT, null, 'a'),
                    new LongOpt("desc", LongOpt.REQUIRED_ARGUMENT, null, 'd')
            };

            Getopt getopt = new Getopt("args", this.params, "", longOpts);

            requirementFilter.setList(this.requirementManager.getAllRequirements());
            this.handleArguments(getopt);

            this.list = requirementFilter.apply();
        } else {
            this.list = this.requirementManager.getAllRequirements();
        }

        this.handleOutput();
    }

    //@req(REQ-142)
    public String getSummary() {
        StringBuilder outputBuilder = new StringBuilder();

        // summary:
        outputBuilder.append("TYRE version: ");
        outputBuilder.append(ConfigurationManager.getInstance().getVersion());
        outputBuilder.append("\n");

        outputBuilder.append("Local storage file: ");
        outputBuilder.append(ConfigurationManager.getInstance().getLocalCacheFilename());
        outputBuilder.append("\n\n");

        return outputBuilder.toString();
    }

    /**
     * Creates a properly formatted string to {@link #output} using the {@link #list} as a base.
     */
    private void handleOutput() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(getSummary());
        for (Requirement req : this.list) {
            stringBuilder.append(getRequirementInfo(req));
        }
        this.output = stringBuilder.toString();
    }

    @Override
    public String getOutput() {
        return this.output;
    }

    //@req(REQ-138)
    @Override
    public String getCommandName() {
        return "cache-find";
    }

}
