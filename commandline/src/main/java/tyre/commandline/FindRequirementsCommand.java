package tyre.commandline;

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;
import tyre.commons.configuration.ConfigurationManager;
import tyre.commons.parser.FileParser;
import tyre.commons.report.HTMLReporter;
import tyre.commons.requirements.management.RequirementManager;
import tyre.commons.requirements.model.FileReference;
import tyre.commons.requirements.model.ParserEntry;
import tyre.commons.requirements.model.Requirement;
import tyre.commons.ui.UIMessagePrompt;
import tyre.commons.utils.filtering.Visitor;
import tyre.commons.utils.filtering.parserEntry.ParserEntryFinder;
import tyre.commons.utils.merger.RequirementMerger;
import tyre.commons.validation.ValidationProcessor;
import tyre.commons.validation.ValidationStatus;
import tyre.commons.validation.ValidationTemplateParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Command class responsible for listing, filtering and sorting file parsing results.
 */
//@req(REQ-70)
public class FindRequirementsCommand extends AbstractCommand {

    /**
     * Format of requirement file occurrence data.
     */
    //@req(REQ-135)
    public static final String REFERENCE_FORMAT = "%s\t.(%s:%s)%n";

    /**
     * String that will be shown to user when there is no version assigned to requirement.
     */
    public static final String NO_VERSION_MESSAGE = "<no version>";

    /**
     * Returns a properly formatted string with info about requirement in a proper format for output.
     * @param requirement Requirement to be displayed.
     * @return Properly formatted string.
     */
    //@req(REQ-141)
    public static String getRequirementInfo(Requirement requirement) {
        StringBuilder reqInfoBuilder = new StringBuilder();
        reqInfoBuilder.append(String.format("ID: %s%n", requirement.getId()));
        reqInfoBuilder.append(String.format("Version: %s%n",
                requirement.getVersion() == null || requirement.getVersion().equals(FileParser.NO_VERSION)
                        ? NO_VERSION_MESSAGE : requirement.getVersion()));
        if (requirement.getTitle() != null && !requirement.getTitle().isEmpty())
            reqInfoBuilder.append(String.format("Title: %s%n", requirement.getTitle()));
        if (requirement.getLocation() != null && !requirement.getLocation().isEmpty())
            reqInfoBuilder.append(String.format("Documents:%n\t%s%n", requirement.getLocation()));
        return reqInfoBuilder.toString();
    }

    public static String getReferencesInfo(List<FileReference> referenceList) {
        if (!referenceList.isEmpty()){
            StringBuilder refInfoBuilder = new StringBuilder();
            refInfoBuilder.append(String.format("Files: %n"));
            for (FileReference reference : referenceList) {
                String filePath = getReferenceFilePath(reference.getFilePath(), reference.getFileName(), reference.getLineNumber());
                refInfoBuilder.append(String.format("\t%s",filePath));
            }
            refInfoBuilder.append(String.format("%n%n"));
            return refInfoBuilder.toString();
        }
        return "";
    }

    public static String getValidationStatus(ValidationStatus entryStatus) {
        if(entryStatus != ValidationStatus.UNKNOWN) {
            String status = "Validation status: ";

            switch(entryStatus) {
                case NOT_IMPLEMENTED:
                    status += "Not implemented.\n";
                    break;

                case PARTIALLY_IMPLEMENTED:
                    status += "Partially implemented.\n";
                    break;

                case FULLY_IMPLEMENTED:
                    status += "Fully implemented.\n";
                    break;
            }

            return status;
        }

        return "";
    }

    /**
     * Returns a properly formatted string with info about given reference to file.
     * @param path Path of file in which requirement was found.
     * @param fileName Filename of parsed file.
     * @param lineNumber Number of line on which the requirement was found.
     * @return Properly formatted string.
     */
    public static String getReferenceFilePath(String path, String fileName, int lineNumber) {
        return String.format(FindRequirementsCommand.REFERENCE_FORMAT,
                path, fileName, lineNumber);
    }

    /**
     * List of filtered/sorted requirements.
     */
    private List<ParserEntry> list;

    /**
     * Filtering/sorting object.
     */
    private ParserEntryFinder parserEntryFinder;

    /**
     * Output string.
     */
    private String output;

    private RequirementMerger merger;

    private RequirementManager requirementManager;

    private boolean report = false;

    private boolean validate = false;

    /**
     * Constructor that takes reference to requirement provider.
     * @param manager reference to requirement manager.
     * @param ui UI utilities used by main program.
     */
    public FindRequirementsCommand(RequirementManager manager, UIMessagePrompt ui) {
        this.merger = new RequirementMerger(manager, ui);
        this.requirementManager = manager;
    }

    /**
     * Gets visitor that is tied with argument provided with sorting flag. If argument does not match any of field names
     * it sorts by id by default.
     * @param argument argument provided with sorting flag
     * @return visitor that is matched with provided argument.
     */
    private Visitor<ParserEntry, String> getVisitor(String argument) {
        if ("version".equals(argument)) {
            return ParserEntryFinder.versionVisitor;
        } else if ("id".equals(argument)) {
            return ParserEntryFinder.idVisitor;
        }
        return ParserEntryFinder.idVisitor;           /* sort by id by default */
    }

    /**
     * Parses string provided as parameter by splitting it on ',' character and returns list of split strings.
     * @param input input string that will be split
     * @return extension list
     */
    private List<String> parseExtensions(String input) {
        List<String> output = new ArrayList<String>();
        String extensions[] = input.split(",");
        if (extensions.length > 0) {
            output.addAll(Arrays.asList(extensions));
        }
        return output;
    }

    /**
     * Handle arguments that were passed to the command.
     * @param getopt Getopt object that contains the array of parameters.
     */
    private void handleArguments(Getopt getopt) {
        int c;
        while ((c = getopt.getopt()) != -1) {
            switch (c) {
                case 'i':
                    parserEntryFinder.addFilter(getopt.getOptarg(), ParserEntryFinder.idVisitor);
                    break;
                case 'v':
                    parserEntryFinder.addFilter(getopt.getOptarg(), ParserEntryFinder.versionVisitor);
                    break;
                case 'a':
                    parserEntryFinder.setSortAscending(getVisitor(getopt.getOptarg()));
                    break;
                case 'd':
                    parserEntryFinder.setSortDescending(getVisitor(getopt.getOptarg()));
                    break;
                case 'p':
                    parserEntryFinder.setPath(getopt.getOptarg());
                    break;
                case 'e':
                    parserEntryFinder.setExtensions(parseExtensions(getopt.getOptarg()));
                    break;
                case 'r':
                    this.report = true;
                    break;
                case 'l':
                    this.validate = true;
                    break;
                default:
                    break;
            }
        }

        // when there is a non-flag option at the end, we will filter by id
        if(getopt.getOptind() < this.params.length) {
            String[] splitParameter = this.params[getopt.getOptind()].split(":");

            parserEntryFinder.addFilter(splitParameter[0], ParserEntryFinder.idVisitor);
            if(splitParameter.length == 2) {
                parserEntryFinder.addFilter(splitParameter[1], ParserEntryFinder.versionVisitor);
            }
        }
    }

    /**
     * Initializes object fields values.
     */
    private void initialize() {
        this.list = null;
        this.parserEntryFinder = new ParserEntryFinder();
        this.output = null;
        this.report = false;
    }

    //@req(REQ-142)
    public String getSummary() {
        StringBuilder outputBuilder = new StringBuilder();

        // summary:
        outputBuilder.append("TYRE version: ");
        outputBuilder.append(ConfigurationManager.getInstance().getVersion());
        outputBuilder.append("\n");

        outputBuilder.append("Local storage file: ");
        outputBuilder.append(ConfigurationManager.getInstance().getLocalCacheFilename());
        outputBuilder.append("\n");

        outputBuilder.append("Search path(s): ");
        int i = 0;
        for(String path : this.parserEntryFinder.getPaths()) {
            outputBuilder.append(path);
            if(i++ < this.parserEntryFinder.getPaths().size() - 1)
                outputBuilder.append("; ");
        }
        outputBuilder.append("\n");

        i = 0;
        outputBuilder.append("Extension(s): ");
        for(String extension : this.parserEntryFinder.getExtensions()) {
            outputBuilder.append(extension);
            if(i++ < this.parserEntryFinder.getExtensions().size() - 1)
                outputBuilder.append(", ");
        }
        outputBuilder.append("\n\n");

        // TODO Add summary about providers after they are refactored

        return outputBuilder.toString();
    }

    /**
     * Creates a properly formatted string to {@link #output} using the {@link #list} as a base.
     */
    private void handleOutput() {
        StringBuilder outputBuilder = new StringBuilder();

        outputBuilder.append(getSummary());

        for (ParserEntry entry : this.list) {
            // try to fetch requirements from manager
            Requirement requirement = merger.fetchRequirement(entry);
            if (requirement == null) {
                requirement = new Requirement(entry.getId(), entry.getVersion(), "", "");
            }
            outputBuilder.append(getRequirementInfo(requirement));
            if(this.validate) {
                outputBuilder.append(getValidationStatus(entry.getValidationStatus()));
            }
            outputBuilder.append(getReferencesInfo(entry.getReferences()));
        }
        this.output = outputBuilder.toString();
    }

    /**
     * Processes the arguments and then filters, sorts or lists the requirements accordingly.
     */
    //@req(REQ-89)
    @Override
    protected void process() {
        this.initialize();
        if (this.params.length > 0) {
            // handle arguments
            LongOpt[] longOpts = {
                    new LongOpt("id", LongOpt.REQUIRED_ARGUMENT, null, 'i'),
                    new LongOpt("version", LongOpt.REQUIRED_ARGUMENT, null, 'v'),
                    new LongOpt("path", LongOpt.REQUIRED_ARGUMENT, null, 'p'),
                    new LongOpt("extensions", LongOpt.REQUIRED_ARGUMENT, null, 'e'),
                    new LongOpt("asc", LongOpt.REQUIRED_ARGUMENT, null, 'a'),
                    new LongOpt("desc", LongOpt.REQUIRED_ARGUMENT, null, 'd'),
                    new LongOpt("report", LongOpt.NO_ARGUMENT, null, 'r'),
                    new LongOpt("validate", LongOpt.NO_ARGUMENT, null, 'l')
            };

            Getopt getopt = new Getopt("args", this.params, "", longOpts);

            this.handleArguments(getopt);
        }

        this.list = parserEntryFinder.apply();
        // @req(REQ-106)
        if (this.validate) {
            new ValidationProcessor(this.list, new ValidationTemplateParser()).process();
        }
        this.handleOutput();

        if (this.report) {
            new HTMLReporter(
                    requirementManager,
                    this.list,
                    parserEntryFinder.getPaths(),
                    parserEntryFinder.getExtensions(),
                    this.validate
            );
        }
    }

    @Override
    public String getCommandName() {
        return "find";
    }

    @Override
    public String getOutput() {
        return this.output;
    }
}
