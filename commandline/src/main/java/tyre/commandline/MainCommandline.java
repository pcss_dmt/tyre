package tyre.commandline;

import tyre.commons.externalData.ProvidersManager;
import tyre.commons.externalData.provider.CommandlineProvider;
import tyre.commons.requirements.management.RequirementManager;
import tyre.commons.requirements.management.RequirementProvider;
import tyre.commons.ui.LoginCredentials;
import tyre.commons.ui.UIMessagePrompt;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main entry point for the console application.
 */
//@req(REQ-89)
public class MainCommandline implements UIMessagePrompt {
    private static final String PROMPT = "TYRE>> ";
    private static final Logger LOGGER = Logger.getLogger(MainCommandline.class.getPackage().getName());
    private InputStream inStream;
    private OutputStream outStream;
    private RequirementManager manager = new RequirementManager();
    private ProvidersManager providersManager;
    private CommandlineProvider commandlineProvider = new CommandlineProvider();

    /**
     * List of all the command objects that will be handled by the application.
     * All of them should extend AbstractCommand class.
     */
    private AbstractCommand[] commands = {
            new CacheFindRequirementsCommand(manager),
            new FindRequirementsCommand(manager, this),
            new ListRequirementsCommand(manager),
            new CacheCreateRequirementsCommand(commandlineProvider, manager),
            new HelpCommand(),
            new VersionCommand()
    };

    private MainCommandline() {
        this.inStream = System.in;
        this.outStream = System.out;

        this.providersManager = new ProvidersManager(this);
        this.providersManager.setCommandlineProvider(this.commandlineProvider);

        for (final RequirementProvider provider : this.providersManager.getProviders()) {
            System.out.println(provider.getProviderType());
            this.manager.addProvider(provider);
        }
    }

    /**
     * Writes string to output stream (and also handles the exception).
     *
     * @param what string that will be written to output stream
     */
    private void writeToOutputStream(String what) {
        try {
            this.outStream.write(what.getBytes());
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Writing stream error!", e);
        }
    }

    /**
     * Handles running of a command.
     * runCommand also trims the arguments passed so they don't contain the name of the command itself.
     *
     * @param command Command object to be run.
     * @param args    Arguments passed by user.
     */
    private void runCommand(AbstractCommand command, String[] args) {
        String[] tmp;
        // we pass only the parameters to command itself
        if (args.length > 1) {
            tmp = Arrays.copyOfRange(args, 1, args.length);
        } else { // ...or an empty array
            tmp = new String[0];
        }

        // run the command itself
        command.run(tmp);

        // output the results
        this.writeToOutputStream(command.getOutput());

    }

    /**
     * Interprets the first part of arguments passed by user and searches through all known commands.
     * If no matching command name is found, an error is shown.
     *
     * @param args Arguments passed by user.
     */
    private void handleCommands(String[] args) {
        for (AbstractCommand command : this.commands) {
            if (args[0].equals(command.getCommandName())) {
                this.runCommand(command, args);

                // command already handled, no need to search through the rest of commands
                return;
            }
        }

        String error = "Unknown command: " + args[0] + "\n";

        this.writeToOutputStream(error);

    }

    /**
     * Helper method that parses a String into String[] consisting of parameters
     * delimited by space character.
     * Handles commands with spaces written in quotes ( " and ' ).
     *
     * @param str String to be parsed.
     * @return An array of parsed parameters.
     */
    private String[] parseCommands(String str) {
        List<String> list = new ArrayList<String>();

        //@req(REQ-134)
        Pattern regex = Pattern.compile("([a-zA-Z\\-=]*(?:[^\\s\"']+|\"[^\"]*\"|'[^']*'))");
        Matcher regexMatcher = regex.matcher(str);
        String buffer;

        while (regexMatcher.find()) {
            if (regexMatcher.group(1) != null) {
                buffer = regexMatcher.group(1).replaceAll("['\"]", "");
                list.add(buffer);
            }
        }

        return list.toArray(new String[list.size()]);
    }

    /**
     * Shows a prompt and waits for a next line to be passed into stream.
     *
     * @param scanner Scanner that handles getting of a line.
     * @return True if next line has been entered.
     */
    private boolean waitForNextLineWithPrompt(Scanner scanner) {
        this.writeToOutputStream(PROMPT);

        return scanner.hasNextLine();
    }

    /**
     * Method invoking interactive mode in a console.
     */
    private void interactiveMode() {
        Scanner scanner = new Scanner(this.inStream);
        String buffer;
        String[] params;

        while (this.waitForNextLineWithPrompt(scanner)) {
            buffer = scanner.nextLine();

            params = this.parseCommands(buffer);

            if (params.length > 0) {
                // special case - exit
                if ("exit".equals(params[0]))
                    return;

                // handle normal commands
                this.handleCommands(params);
            }
        }
    }

    /**
     * Checks whether or not user has passed any parameters.
     * If there are any parameters, non-interactive mode is used,
     * otherwise, interactive mode is used.
     *
     * @param args Arguments passed by user.
     */
    public void run(String[] args) {
        if (args.length > 0)
            this.handleCommands(args);
        else
            this.interactiveMode();
    }

    /**
     * Main method of the application.
     *
     * @param args Arguments from commandline.
     */
    public static void main(String[] args) {
        MainCommandline mainObject = new MainCommandline();
        mainObject.run(args);
    }

    @Override
    public void print(String message) {
        writeToOutputStream(message);
    }

    @Override
    public void showOKPrompt(String message) {
        writeToOutputStream(message);
        try {
            this.inStream.read();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Something went wrong!", e);
        }
    }

    @Override
    public boolean showYesNoPrompt(String message) {
        Scanner scanner = new Scanner(this.inStream);

        writeToOutputStream(message);
        if (scanner.hasNextLine()) {
            String response = scanner.nextLine().toLowerCase();
            if ("yes".equals(response) || "y".equals(response)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public LoginCredentials showLoginPrompt(String message) {
        Scanner scanner = new Scanner(this.inStream);
        LoginCredentials credentials = new LoginCredentials();

        writeToOutputStream(message);
        writeToOutputStream("Username:\n");
        if (scanner.hasNextLine()) {
            credentials.setUsername(scanner.nextLine());
        }

        writeToOutputStream("Password:\n");
        credentials.setPassword(new String(System.console().readPassword()));

        return credentials;
    }
}
