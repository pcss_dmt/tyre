package tyre.commandline;

/**
 * Command that shows the help message.
 */
public class HelpCommand extends AbstractCommand {
    @Override
    protected void process() {
        // There's no need to process anything.
    }

    @Override
    public String getCommandName() {
        return "help";
    }

    @Override
    public String getOutput() {
        return "TYRE Requirement Tracker\n\nUsage:\n"
                + "tyre [command [params...]]\n\n"
                + "Commands:\n"
                + "help\t- shows this help message\n"
                + "version\t- shows version of TYRE\n"
                + "find\t- finds requirements in provided storage\n"
                + "\t  Accepted options:\n"
                + "\t   --id=PATTERN - filters requirements by id matching PATTERN\n"
                + "\t   --version=PATTERN - filters requirements by version matching PATTERN\n"
                + "\t   --asc=FIELD - sorts the list in ascending order by FIELD (id, version)\n"
                + "\t   --desc=FIELD - sorts the list in descending order by FIELD (id, version)\n"
                + "\t   --path=PATH - parses all the files in given PATH and recursively finds requirements in it\n"
                + "\t   --extensions=EXT - tells the parser to parse only files with given EXT. Multiple extensions can be separated by a comma (,)\n"
                + "\t   --report - generates HTML report.\n "
                + "\t   --validate - validates the files."
                + "\t   ID[:VERSION] - finds requirement with ID and/or VERSION. Must be provided at the end of the command.\n "
                + "cache-find\t- finds requirements in local storage\n"
                + "\t  Accepted options:\n"
                + "\t   --id=PATTERN - filters requirements by id matching PATTERN\n"
                + "\t   --title=PATTERN - filters requirements by title matching PATTERN\n"
                + "\t   --version=PATTERN - filters requirements by version matching PATTERN\n"
                + "\t   --location=PATTERN - filters requirements by location matching PATTERN\n"
                + "\t   --asc=FIELD - sorts the list in ascending order by FIELD (id, title, version, content)\n"
                + "\t   --desc=FIELD - sorts the list in descending order by FIELD (id, title, version, content)\n"
                + "\t   ID[:VERSION] - finds requirement with ID and/or VERSION. Must be provided at the end of the command.\n "
                + "cache-create\t- creates the requirement\n"
                + "\t  Required options:\n"
                + "\t   --id=ID - id of the requirement\n"
                + "\t   --title=TITLE - title of the requirement\n"
                + "\t   --version=VERSION - version of the requirement\n"
                + "\t   --location=LOCATION - location of the requirement\n"
                + "list\t- lists all the requirements stored in local cache\n";
    }
}
