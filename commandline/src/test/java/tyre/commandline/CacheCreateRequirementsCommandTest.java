package tyre.commandline;

import org.junit.Before;
import org.junit.Test;
import java.util.List;

import tyre.commons.externalData.provider.CommandlineProvider;
import tyre.commons.requirements.management.ProviderInternalErrorException;
import tyre.commons.requirements.management.RequirementManager;
import tyre.commons.requirements.model.Requirement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class CacheCreateRequirementsCommandTest {
    private RequirementManager manager;
    private CommandlineProvider commandlineProvider;
    private CacheCreateRequirementsCommand CUT;

    @Before
    public void initialize() {
        manager = new RequirementManager();
        manager.getRequirementStorage().clear();
        commandlineProvider = new CommandlineProvider();
        CUT = new CacheCreateRequirementsCommand(commandlineProvider,manager);
    }

    @Test
    public void createValidRequirementTest() throws ProviderInternalErrorException {
        String[] params = {"--id=1", "--version=2.0", "--location=www.example.com", "--title=Test"};

        CUT.run(params);

        assertEquals(new Requirement("1", "2.0", "Test", "www.example.com"), commandlineProvider.getById("1").get(0));

    }

    @Test
    public void createRequirementWithWrongArgumentsTest() throws ProviderInternalErrorException {
        String params[] = {"--id=1", "--version=2.0", "--url=www.example.com", "--title=Test"};

        CUT.run(params);

        assertTrue(commandlineProvider.getById("1").isEmpty());
        assertEquals("This option is not valid\n", CUT.getOutput());
    }

    @Test
    public void createRequirementWithMissingArgumentsTest() throws ProviderInternalErrorException {
        String params[] = {"--location=www.example.com", "--title=Test"};

        CUT.run(params);

        boolean requirementWithNullId = false;
        List<Requirement> requirements = commandlineProvider.getAllRequirements();

        for (Requirement req : requirements){
            if (req.getId() == null)
                requirementWithNullId = true;
        }

        assertFalse(requirementWithNullId);
    }

}