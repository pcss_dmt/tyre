package tyre.commandline;

import org.junit.*;
import tyre.commons.requirements.management.RequirementManager;
import tyre.commons.requirements.management.RequirementProvider;
import tyre.commons.requirements.model.Requirement;

import java.util.ArrayList;
import java.util.List;

public class CacheFindRequirementsCommandTest {

    private CacheFindRequirementsCommand CUT;
    private Requirement[] requirements = {
            new Requirement("bbb", "10", "Test req 2", "http://www.site.com/req2"),
            new Requirement("aaa", "5", "Test req 1", "http://www.site.com/req1"),
            new Requirement("ddd", "17", "Test req 3", "http://www.site.com/req3"),
            new Requirement("ccc", "1", "Test req 4", "http://www.site.com/req4")
    };

    private class MockProvider implements RequirementProvider {

        private List<Requirement> requirements = new ArrayList<Requirement>();

        public List<Requirement> getAllRequirements() {
            return this.requirements;
        }

        public List<Requirement> getById(String id) {
            List<Requirement> reqs = new ArrayList<Requirement>();

            for(Requirement req : this.requirements) {
                if(req.getId().equals(id))
                    reqs.add(req);
            }

            return reqs;
        }

        public String getProviderType() {
            return "MockProvider";
        }

        public boolean addRequirement(Requirement requirement) {
            this.requirements.add(requirement);

            return true;
        }
    }

    @Before
    public void before() {
        MockProvider mp = new MockProvider();

        mp.addRequirement(requirements[0]);
        mp.addRequirement(requirements[1]);
        mp.addRequirement(requirements[2]);
        mp.addRequirement(requirements[3]);

        RequirementManager requirementManager = new RequirementManager();
        requirementManager.getRequirementStorage().clear();
        requirementManager.addProvider(mp);

        this.CUT = new CacheFindRequirementsCommand(requirementManager);
    }

    @Test
    public void showAllTest() {
        boolean test;
        String params[] = {};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[1]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]));

        Assert.assertTrue("Show all requirements fail", test);
    }

    @Test
    public void shouldFindRequirementsById() {
        boolean test;
        String params[] = {"--id=aaa"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
		CacheFindRequirementsCommand.getRequirementInfo(requirements[1]));

        Assert.assertTrue("Filter by id fail", test);
    }

    @Test
    public void shouldFindRequirementsByIdRegex() {
        boolean test;
        String params[] = {"--id=[a-b]+"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[1]));

        Assert.assertTrue("Filter by id regex fail", test);
    }

    @Test
    public void shouldFindRequirementsByTitle() {
        boolean test;
        String params[] = {"--title=Test req 4"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
		CacheFindRequirementsCommand.getRequirementInfo(requirements[3]));

        Assert.assertTrue("Filter by title fail", test);
    }

    @Test
    public void shouldFindRequirementsByTitleRegex() {
        boolean test;
        String params[] = {"--title=Test req [23]{1}"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]));

        Assert.assertTrue("Filter by title regex fail", test);
    }

    @Test
    public void shouldFindRequirementsByVersion() {
        boolean test;
        String params[] = {"--version=17"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
		CacheFindRequirementsCommand.getRequirementInfo(requirements[2]));

        Assert.assertTrue("Filter by version fail", test);
    }

    @Test
    public void shouldFindRequirementsByVersionRegex() {
        boolean test;
        String params[] = {"--version=(10|5)"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
		CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[1]));

        Assert.assertTrue("Filter by version regex fail", test);
    }

    @Test
    public void shouldFindRequirementsByLocation() {
        boolean test;
        String params[] = {"--location=http://www.site.com/req1"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
		CacheFindRequirementsCommand.getRequirementInfo(requirements[1]));

        Assert.assertTrue("Filter by location fail", test);
    }

    @Test
    public void shouldFindRequirementsByLocationRegex() {
        boolean test;
        String params[] = {"--location=.*req1"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
		CacheFindRequirementsCommand.getRequirementInfo(requirements[1]));

        Assert.assertTrue("Filter by location regex fail", test);
    }

    @Test
    public void shouldSortRequirementsAscendingById() {
        boolean test;
        String params[] = {"--asc=id"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
		CacheFindRequirementsCommand.getRequirementInfo(requirements[1]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]));

        Assert.assertTrue("Sort ascending by id regex fail", test);
    }

    @Test
    public void shouldSortRequirementsAscendingByIdWithFilter() {
        boolean test;
        String params[] = {"--id=[bd]+", "--asc=id"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]));

        Assert.assertTrue("Sort ascending by id with filter regex fail", test);
    }

    @Test
    public void shouldSortRequirementsDescendingById() {
        boolean test;
        String params[] = {"--desc=id"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[1]));

        Assert.assertTrue("Sort descending by id fail", test);
    }

    @Test
    public void shouldSortRequirementsDescendingByIdWithFilter() {
        boolean test;
        String params[] = {"--id=[bd]+", "--desc=id"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]));

        Assert.assertTrue("Sort descending by id with filter regex fail", test);
    }

    @Test
    public void shouldSortRequirementsAscendingByTitle() {
        boolean test;
        String params[] = {"--asc=title"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[1]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]));

        Assert.assertTrue("Sort ascending by title fail", test);
    }

    @Test
    public void shouldSortRequirementsAscendingByTitleWithFilter() {
        boolean test;
        String params[] = {"--title=Test req [23]{1}", "--asc=title"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]));

        Assert.assertTrue("Sort ascending by title with filter regex fail", test);
    }

    @Test
    public void shouldSortRequirementsDescendingByTitle() {
        boolean test;
        String params[] = {"--desc=title"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[1]));

        Assert.assertTrue("Sort descending by title fail", test);
    }

    @Test
    public void shouldSortRequirementsDescendingByTitleWithFilter() {
        boolean test;
        String params[] = {"--title=Test req [23]{1}", "--desc=title"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]));

        Assert.assertTrue("Sort descending by title with filter regex fail", test);
    }

    @Test
    public void shouldSortRequirementsAscendingByVersion() {
        boolean test;
        String params[] = {"--asc=version"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[1]));

        Assert.assertTrue("Sort ascending by version regex fail", test);
    }

    @Test
    public void shouldSortRequirementsAscendingByVersionWithFilter() {
        boolean test;
        String params[] = {"--version=(10|17)", "--asc=version"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]));

        Assert.assertTrue("Sort ascending by version with filter regex fail", test);
    }

    @Test
    public void shouldSortRequirementsDescendingByVersion() {
        boolean test;
        String params[] = {"--desc=version"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[1]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]));

        Assert.assertTrue("Sort descending by version regex fail", test);
    }

    @Test
    public void shouldSortRequirementsDescendingByVersionWithFilter() {
        boolean test;
        String params[] = {"--version=(10|17)", "--desc=version"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]));

        Assert.assertTrue("Sort descending by version with filter regex fail", test);
    }

    @Test
    public void shouldSortRequirementsAscendingByLocation() {
        boolean test;
        String params[] = {"--asc=location"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[1]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]));

        Assert.assertTrue("Sort ascending by location fail", test);
    }

    @Test
    public void shouldSortRequirementsAscendingByLocationWithFilter() {
        boolean test;
        String params[] = {"--location=.*(req3|req4)", "--asc=location"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]));

        Assert.assertTrue("Sort ascending by location with filter regex fail", test);
    }

    @Test
    public void shouldSortRequirementsDescendingByLocation() {
        boolean test;
        String params[] = {"--desc=location"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[0]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[1]));

        Assert.assertTrue("Sort descending by location fail", test);
    }

    @Test
    public void shouldSortRequirementsDescendingByLocationWithFilter() {
        boolean test;
        String params[] = {"--location=.*(req3|req4)", "--desc=location"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[3]) +
                CacheFindRequirementsCommand.getRequirementInfo(requirements[2]));

        Assert.assertTrue("Sort descending by location with filter regex fail", test);
    }

    @Test
    public void shouldFindRequirementsWithMultipleFilters() {
        boolean test;
        String params[] = {"--id=[ab]+", "--title=Test req (1|3)"};
        this.CUT.run(params);
        String result = this.CUT.getOutput();

        test = result.equals(this.CUT.getSummary() +
		CacheFindRequirementsCommand.getRequirementInfo(requirements[1]));

        Assert.assertTrue("Multiple filters fail", test);
    }

}
