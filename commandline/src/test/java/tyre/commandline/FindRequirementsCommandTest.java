package tyre.commandline;

import org.apache.commons.io.FilenameUtils;
import org.junit.*;
import tyre.commons.parser.FileParser;
import tyre.commons.requirements.management.RequirementManager;
import tyre.commons.requirements.management.RequirementProvider;
import tyre.commons.requirements.model.Requirement;
import tyre.commons.requirements.model.FileReference;
import tyre.commons.ui.LoginCredentials;
import tyre.commons.ui.UIMessagePrompt;

import java.util.ArrayList;
import java.util.List;

public class FindRequirementsCommandTest {

    private final String pathToTestDir = FilenameUtils.separatorsToSystem(
            System.getProperty("user.dir")
            + "/src/test/resources/findRequirementsTestFiles/"
    );
    private final String testFile = FilenameUtils.separatorsToSystem(
            pathToTestDir + "test.txt"
    );
    private final String testFileName = "test.txt";

    private FindRequirementsCommand CUT;
    private Requirement retrievedRequirement = new Requirement(
            "aaa", "5", "Test req 1", "http://www.site.com/req1"
    );
    private Requirement notRetrievedRequirement = new Requirement(
            "ddd", FileParser.NO_VERSION, "", ""
    );

    private ArrayList<FileReference> retrievedRequirementReferences = new ArrayList<FileReference>() {{
        add(new FileReference(testFile, 2));
    }};

    private ArrayList<FileReference> notRetrievedRequirementReferences = new ArrayList<FileReference>() {{
        add(new FileReference(testFile, 4));
    }};

    private class MockUI implements UIMessagePrompt {

        @Override
        public void print(String message) {

        }

        @Override
        public void showOKPrompt(String message) {

        }

        @Override
        public boolean showYesNoPrompt(String message) {
            return false;
        }

        @Override
        public LoginCredentials showLoginPrompt(String message) {
            return null;
        }
    }

    private class MockProvider implements RequirementProvider {

        private List<Requirement> requirements = new ArrayList<Requirement>();

        public List<Requirement> getAllRequirements() {
            return this.requirements;
        }

        public List<Requirement> getById(String id) {
            List<Requirement> reqs = new ArrayList<Requirement>();

            for(Requirement req : this.requirements) {
                if(req.getId().equals(id))
                    reqs.add(req);
            }

            return reqs;
        }

        public String getProviderType() {
            return "MockProvider";
        }

        public boolean addRequirement(Requirement requirement) {
            this.requirements.add(requirement);

            return true;
        }

        public boolean synchronize() {
            return true;
        }
    }

    @Before
    public void before() {
        MockProvider mp = new MockProvider();
        MockUI ui = new MockUI();

        mp.addRequirement(retrievedRequirement);

        RequirementManager requirementManager = new RequirementManager();
        requirementManager.getRequirementStorage().clear();
        requirementManager.addProvider(mp);


        this.CUT = new FindRequirementsCommand(requirementManager, ui);
    }


    @Test
    public void shouldReturnAll() {
        boolean test;
        String[] params = {"--path=" + pathToTestDir, "--extensions=txt"};
        this.CUT.run(params);

        String output =this.CUT.getOutput();
        String expectedOutput = this.CUT.getSummary() +
		FindRequirementsCommand.getRequirementInfo(retrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(retrievedRequirementReferences) +
                FindRequirementsCommand.getRequirementInfo(notRetrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(notRetrievedRequirementReferences);
        test = output.equals(expectedOutput);

        Assert.assertTrue("shouldReturnAll assert fail.", test);
    }

    @Test
    public void shouldReturnRequirementsSortedAscById() {
        boolean test;
        String[] params = {"--path=" + pathToTestDir, "--extensions=txt",
                "--asc=id"};
        this.CUT.run(params);

        String output =this.CUT.getOutput();
        String expectedOutput = this.CUT.getSummary() +
		FindRequirementsCommand.getRequirementInfo(retrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(retrievedRequirementReferences) +
                FindRequirementsCommand.getRequirementInfo(notRetrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(notRetrievedRequirementReferences);
        test = output.equals(expectedOutput);

        Assert.assertTrue("shouldReturnRequirementsSortedAscById assert fail.",
                test);
    }

    @Test
    public void shouldReturnRequirementsSortedDescById() {
        boolean test;
        String[] params = {"--path=" + pathToTestDir, "--extensions=txt",
                "--desc=id"};
        this.CUT.run(params);

        String output =this.CUT.getOutput();
        String expectedOutput = this.CUT.getSummary() +
		FindRequirementsCommand.getRequirementInfo(notRetrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(notRetrievedRequirementReferences)+
                FindRequirementsCommand.getRequirementInfo(retrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(retrievedRequirementReferences);
        test = output.equals(expectedOutput);

        Assert.assertTrue("shouldReturnRequirementsSortedDescById assert fail.",
                test);
    }

    @Test
    public void shouldReturnRequirementsSortedAscByVersion() {
        boolean test;
        String[] params = {"--path=" + pathToTestDir, "--extensions=txt",
                "--asc=version"};
        this.CUT.run(params);

        String output =this.CUT.getOutput();
        String expectedOutput = this.CUT.getSummary() +
		FindRequirementsCommand.getRequirementInfo(notRetrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(notRetrievedRequirementReferences)+
                FindRequirementsCommand.getRequirementInfo(retrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(retrievedRequirementReferences);
        test = output.equals(expectedOutput);

        Assert.assertTrue("shouldReturnRequirementsSortedAscByVersion assert fail.",
                test);
    }

    @Test
    public void shouldReturnRequirementsSortedDescByVersion() {
        boolean test;
        String[] params = {"--path=" + pathToTestDir, "--extensions=txt",
                "--desc=version"};
        this.CUT.run(params);

        String output =this.CUT.getOutput();
        String expectedOutput = this.CUT.getSummary() +
		FindRequirementsCommand.getRequirementInfo(retrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(retrievedRequirementReferences) +
                FindRequirementsCommand.getRequirementInfo(notRetrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(notRetrievedRequirementReferences);
        test = output.equals(expectedOutput);

        Assert.assertTrue("shouldReturnRequirementsSortedDescByVersion assert fail.",
                test);
    }

    @Test
    public void shouldFilterById() {
        boolean test;
        String[] params = {"--path=" + pathToTestDir, "--extensions=txt",
                "--id=ddd"};
        this.CUT.run(params);

        String output =this.CUT.getOutput();
        String expectedOutput = this.CUT.getSummary() +
		FindRequirementsCommand.getRequirementInfo(notRetrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(notRetrievedRequirementReferences);
        test = output.equals(expectedOutput);

        Assert.assertTrue("shouldFilterById assert fail.", test);
    }

    @Test
    public void shouldFilterByVersion() {
        boolean test;
        String[] params = {"--path=" + pathToTestDir, "--extensions=txt",
                "--version=5"};
        this.CUT.run(params);

        String output =this.CUT.getOutput();
        String expectedOutput = this.CUT.getSummary() +
		FindRequirementsCommand.getRequirementInfo(retrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(retrievedRequirementReferences);
        test = output.equals(expectedOutput);

        Assert.assertTrue("shouldFilterByVersion assert fail.", test);
    }

    @Test
    public void shouldFilterByVersionAndId() {
        boolean test;
        String[] params = {"--path=" + pathToTestDir, "--extensions=txt",
                "--version=5", "--id=aaa"};
        this.CUT.run(params);

        String output =this.CUT.getOutput();
        String expectedOutput = this.CUT.getSummary() +
		FindRequirementsCommand.getRequirementInfo(retrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(retrievedRequirementReferences);
        test = output.equals(expectedOutput);

        Assert.assertTrue("shouldFilterByVersionAndId assert fail.", test);
    }

    @Test
    public void shouldFilterByIdShorthand() {
        boolean test;
        String[] params = {"--path=" + pathToTestDir, "--extensions=txt",
                "aaa"};
        this.CUT.run(params);

        String output =this.CUT.getOutput();
        String expectedOutput = this.CUT.getSummary() +
		FindRequirementsCommand.getRequirementInfo(retrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(retrievedRequirementReferences);
        test = output.equals(expectedOutput);

        Assert.assertTrue("shouldFilterByIdShorthand assert fail.", test);
    }

    @Test
    public void shouldFilterByIdAndVersionShorthand() {
        boolean test;
        String[] params = {"--path=" + pathToTestDir, "--extensions=txt",
                "aaa:5"};
        this.CUT.run(params);

        String output =this.CUT.getOutput();
        String expectedOutput = this.CUT.getSummary() +
		FindRequirementsCommand.getRequirementInfo(retrievedRequirement) +
                FindRequirementsCommand.getReferencesInfo(retrievedRequirementReferences);
        test = output.equals(expectedOutput);

        Assert.assertTrue("shouldFilterByIdAndVersionShorthand assert fail.", test);
    }
}