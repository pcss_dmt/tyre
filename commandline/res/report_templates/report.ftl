<!DOCTYPE html>
<html>
	<head>
		<title>TYRE HTML report</title>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<style>
			.tooltip-inner {
            	max-width: none;
            	white-space: nowrap;
        	}
            .icons {
                float: right;
            }
            .name {
                display: inline-block;
            }
            .col-md-1-5{
                width: 10.5%;
            }
            .file-list-item{
                margin-bottom: 6px;
            }
            .tyreversion{
                color: #D4D4D4;
                float: left;
            }
            .generationdate{
                color: #D4D4D4;
                float: right;
            }
            .highlightedvalue{
                color: rgb(149, 133, 122);
            }
            .more-area{
                margin-top: 30px;
            }
            .icons-padding{
                padding-right: 10px;
            }
        </style>
	</head>
	<body>
		<nav class="navbar navbar-default">
		  <div class="container">
			<div class="navbar-inner">
				<h1>TYRE tool report</h1>
				<p class="generationdate highlightedvalue">${current_date}</p><p class="generationdate">generated:&nbsp</p>
                <#if tyreVersion??>
                    <p class="tyreversion">version:&nbsp</p> <p class="tyreversion highlightedvalue">${tyreVersion}</p>
                </#if>
			</div>
		</nav>

		<div class="container">
            <div class="row more-area">
                <#if storageFile??>
                    <div class="col-sm-6 col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="glyphicon glyphicon-file icons-padding"></span>Local storage file</h3>
                            </div>
                            <div class="panel-body">
                                <ul><li>${storageFile}</li></ul>
                            </div>
                        </div>
                    </div>
                </#if>

                <#if paths??>
                    <div class="col-sm-6 col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="glyphicon glyphicon-folder-close icons-padding"></span>Search path</h3>
                            </div>
                            <div class="panel-body">
                                <ul>
                                <#list paths as path>
                                    <li>${path}</li>
                                </#list>
                                </ul>
                            </div>
                        </div>
                    </div>
                </#if>

                <#if extensions??>
                <div class="col-sm-6 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading ">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-cog icons-padding"></span>Extension</h3>
                        </div>
                        <div class="panel-body ">
                            <ul>
                            <#list extensions as extension>
                                <li>${extension}</li>
                            </#list>
                            </ul>
                        </div>
                    </div>
                </div>
                </#if>
            </div>

            <div class="panel panel-default more-area">
                <div class="panel-heading">
                    <h2 class="panel-title">Content</h2>
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-1-5">
                                ID
                            </th>
                            <th>
                                Version
                            </th>
                            <th class="col-md-4">
                                Title
                            </th>
                            <#if validation_enabled>
                                <th class="col-md-1-5">
                                    Validation status
                                </th>
                            </#if>
                            <th class="col-md-5">
                                File references
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <#list results as result>
                            <tr>
                                <td class="col-md-1-5">
                                    ${result.entry.id}
                                    <#if result.requirement??>
                                        <div class="btn-group icons " role="group" >
                                            <a href="${result.requirement.location}" target="_blank" data-toggle="tooltip" data-trigger="hover"  title="${result.requirement.location}" data-placement="left">
                                                <button type="button" class="btn btn-default btn-xs"> <span class="glyphicon glyphicon-link" aria-hidden="true"></span></button>
                                            </a>
                                        </div>
                                    </#if>
                                </td>
                                <td>
                                    <#if result.entry.version == "$NO_VERSION">
                                        No version provided.
                                    <#else>
                                        ${result.entry.version}
                                    </#if>
                                </td>
                                <td class="col-md-4">
                                    <#if result.requirement??>
                                        ${result.requirement.title}
                                    <#else>
                                        Requirement data could not be fetched.
                                    </#if>
                                </td>
                                <#if validation_enabled>
                                    <td class="col-md-1-5">
                                        <#if result.entry.validationStatus.name() == "UNKNOWN">
                                            Unknown
                                        <#elseif result.entry.validationStatus.name() == "NOT_IMPLEMENTED">
                                            Not implemented
                                        <#elseif result.entry.validationStatus.name() == "PARTIALLY_IMPLEMENTED">
                                            Partially implemented
                                        <#elseif result.entry.validationStatus.name() == "FULLY_IMPLEMENTED">
                                            Fully implemented
                                        </#if>
                                    </td>
                                </#if>
                                <td class="col-md-5">
                                    <ul>
                                        <#list result.entry.references as reference>
                                            <li>
                                                <div class="file-list-item">
                                                    <div class="name"><span  data-toggle="tooltip" data-trigger="hover" title="${reference.filePath}">${reference.fileName} line ${reference.lineNumber}</span></div>

                                                    <div class="btn-toolbar icons" role="toolbar" >
                                                        <div class="btn-group " role="group" >
                                                            <a href = "${reference.filePath}" target="blank">
                                                                <button type="button" class="btn btn-default btn-xs"> <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span></button>
                                                            </a>
                                                        </div>
                                                        <#if repositoryUrl??>
                                                            <div class="btn-group " role="group" >
                                                                <a href="${repositoryUrl}${reference.transformedPath}#${reference.lineNumber}" target = "blank" data-placement="left" data-toggle="tooltip" data-trigger="hover" title="${repositoryUrl}${reference.transformedPath}">
                                                                    <button type="button" class="btn btn-default btn-xs"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span></button>
                                                                </a>
                                                            </div>
                                                        </#if>
                                                    </div>
                                                    <br>
                                                </div>
                                            </li>
                                        </#list>
                                    </ul>
                                </td>
                                </tr>
                        </#list>
                    </tbody>
                </table>
            </div>

			<h2><span class="glyphicon glyphicon-warning-sign icons-padding"></span>Warnings</h2>
			<#list warnings as warning>
				<ul>
					<li>${warning}</li>
				</ul>
			</#list>

			<hr>

			<footer>
                <div>
                    <p>TYRE 2016</p>
                </div>
			</footer>
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
		<script>
			$(function () {
			  $('[data-toggle="tooltip"]').tooltip({delay: { show: 0, hide: 750}});
			})
		</script>
	</body>
</html>