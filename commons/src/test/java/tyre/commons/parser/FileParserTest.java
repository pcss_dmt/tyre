package tyre.commons.parser;

import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;
import tyre.commons.requirements.model.FileReference;

import java.util.ArrayList;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class FileParserTest {
    private FileParser CUT;

    private final String pathToTestedFiles = FilenameUtils.separatorsToSystem(System.getProperty("user.dir") + "/src/test/resources/filesToTestParser/");
    private final String fileName1 = pathToTestedFiles + "test1.txt";
    private final String fileName2 = pathToTestedFiles + "test2.txt";
    private final String fileName3 = pathToTestedFiles + "nonExistentFile.txt";

    @Before
    public void before() {
        this.CUT = new FileParser();
    }

    @Test
    public void shouldFindTagsTest() {
        Map<String, ArrayList<FileReference>> hm = this.CUT.parse(fileName1);

        assertTrue(hm.containsKey("req1:" + FileParser.NO_VERSION));
        assertTrue(hm.containsKey("req2:10"));
        assertTrue(hm.containsKey("req3:" + FileParser.NO_VERSION));
    }

    @Test
    public void shouldNotFindTagsTest() {
        Map<String, ArrayList<FileReference>> hm = this.CUT.parse(fileName2);

        assertTrue(hm.isEmpty());
    }

    @Test
    public void shouldReturnProperReferencesTest() {
        Map<String, ArrayList<FileReference>> hm = this.CUT.parse(fileName1);

        ArrayList<FileReference> references = hm.get("req1:" + FileParser.NO_VERSION);
        assertTrue(references.get(0).getFilePath().equals(fileName1));
        assertEquals(1, references.get(0).getLineNumber());

        assertTrue(references.get(1).getFilePath().equals(fileName1));
        assertEquals(6, references.get(1).getLineNumber());
    }

    @Test
    public void shouldHandleExceptions() {
        Map<String, ArrayList<FileReference>> hm = this.CUT.parse(fileName3);

        assertEquals(null, hm);
    }
}