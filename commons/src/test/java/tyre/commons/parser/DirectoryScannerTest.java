package tyre.commons.parser;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;


public class DirectoryScannerTest {

    private final String[] extensionsEmpty = {};
    private final String[] extensionsJava = {"java"};
    private final String[] extensionsJavaCpp = {"java", "cpp"};
    private final String[] extensionsNotUsed = {"bin"};

    private final String projectPath = System.getProperty("user.dir");

    private final String dirPath = FilenameUtils.separatorsToSystem(projectPath + "/src/test/resources/filesToTestDirectoryScanner/");

    private final String[] javaExpectedFiles = {
            dirPath + ".java",
            dirPath + "file.java",
            dirPath + "secondFile.java",
            dirPath + FilenameUtils.separatorsToSystem("firstDir/.java"),
            dirPath + FilenameUtils.separatorsToSystem("firstDir/file.java"),
            dirPath + FilenameUtils.separatorsToSystem("firstDir/secondFile.java"),
            dirPath + FilenameUtils.separatorsToSystem("firstDir/secondDir/thirdDir/file.java")};

    private final String[] cppExpectedFiles = {
            dirPath + ".cpp",
            dirPath + "file.cpp",
            dirPath + "second.cpp",
            dirPath + FilenameUtils.separatorsToSystem("firstDir/.cpp"),
            dirPath + FilenameUtils.separatorsToSystem("firstDir/file.cpp"),
            dirPath + FilenameUtils.separatorsToSystem("firstDir/second.cpp")};

    private final String[] javaFilesInFirstDir = {dirPath + ".java", dirPath + "file.java", dirPath + "secondFile.java"};
    private final String[] cppFilesInFirstDir = {dirPath + ".cpp", dirPath + "file.cpp", dirPath + "second.cpp"};

    private Set<String> expected;
    private DirectoryScanner actual;

    @Test
    public void testGetAllFilesWithExtensionsIfEmptyExtensions() throws Exception {
        expected = new HashSet<String>();
        actual = new DirectoryScanner(dirPath, Arrays.asList(extensionsEmpty));
        assertEquals(expected, actual.getAllFilesWithExtensions());
    }

    @Test
    public void testGetAllFilesWithExtensionsIfNoExtensions() throws Exception {
        expected = new HashSet<String>();
        actual = new DirectoryScanner(dirPath, Arrays.asList(extensionsNotUsed));
        assertEquals(expected, actual.getAllFilesWithExtensions());
    }

    @Test
    public void testGetAllFilesWithExtensionsIfJavaExtensions() throws Exception {
        expected = new HashSet<String>(Arrays.asList(javaExpectedFiles));
        actual = new DirectoryScanner(dirPath, Arrays.asList(extensionsJava));
        assertEquals(expected, actual.getAllFilesWithExtensions());
    }

    @Test
    public void testGetAllFilesWithExtensionsIfTwoExtensions() throws Exception {
        expected = new HashSet<String>();
        expected.addAll(Arrays.asList(javaExpectedFiles));
        expected.addAll(Arrays.asList(cppExpectedFiles));
        actual = new DirectoryScanner(dirPath, Arrays.asList(extensionsJavaCpp));
        assertEquals(expected, actual.getAllFilesWithExtensions());
    }

    @Test
    public void testGetFilesWithExtensionsIfEmptyExtensions() throws Exception {
        expected = new HashSet<String>();
        actual = new DirectoryScanner(dirPath, Arrays.asList(extensionsEmpty));
        assertEquals(expected, actual.getFilesWithExtensions());
    }

    @Test
    public void testGetFilesWithExtensionsIfNoExtensions() throws Exception {
        expected = new HashSet<String>();
        actual = new DirectoryScanner(dirPath, Arrays.asList(extensionsNotUsed));
        assertEquals(expected, actual.getFilesWithExtensions());
    }

    @Test
    public void testGetFilesWithExtensionsIfJavaExtensions() throws Exception {
        expected = new HashSet<String>(Arrays.asList(javaFilesInFirstDir));
        actual = new DirectoryScanner(dirPath, Arrays.asList(extensionsJava));
        assertEquals(expected, actual.getFilesWithExtensions());
    }

    @Test
    public void testGetFilesWithExtensionsIfTwoExtensions() throws Exception {
        expected = new HashSet<String>();
        expected.addAll(Arrays.asList(javaFilesInFirstDir));
        expected.addAll(Arrays.asList(cppFilesInFirstDir));
        actual = new DirectoryScanner(dirPath, Arrays.asList(extensionsJavaCpp));
        assertEquals(expected, actual.getFilesWithExtensions());
    }

}