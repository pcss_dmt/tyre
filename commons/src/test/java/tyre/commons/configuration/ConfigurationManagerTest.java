package tyre.commons.configuration;

import org.junit.*;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class ConfigurationManagerTest {
    private ConfigurationManager CUT = null;

    @Before
    public void initializeEnvironment() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        this.CUT = ConfigurationManager.getInstance();
    }

    @Test
    public void shouldGetLocalCacheFilenameName() {
        Assert.assertEquals("localCache", this.CUT.getLocalCacheFilename());
    }

    @Test
    public void shouldGetTagStartSequence() {
        Assert.assertEquals("@(?i:tyre|requirement|req)", this.CUT.getTagStartSequence());
    }

    @Test
    public void shouldGetVersion() {
        Assert.assertEquals("0.2 Alpha", this.CUT.getVersion());
    }

    @Test
    public void shouldGetPaths() {
        ArrayList<String> extensions = this.CUT.getPaths().get("./");
        String key = (String) this.CUT.getPaths().keySet().toArray()[0];
        org.junit.Assert.assertEquals("./", key);
        org.junit.Assert.assertEquals("java", extensions.get(0));
    }


}