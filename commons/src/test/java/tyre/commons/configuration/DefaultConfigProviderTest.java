package tyre.commons.configuration;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class DefaultConfigProviderTest {

    private DefaultConfigProvider CUT = null;

    @Before
    public void initializeEnvironment() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        this.CUT = new DefaultConfigProvider();
    }

    @Test
    public void shouldGetLocalCacheFilenameName() {
        assertEquals("localCache", this.CUT.getLocalCacheFilename());
    }

    @Test
    public void shouldGetTagStartSequence() {
        assertEquals("@(?i:tyre|requirement|req)", this.CUT.getTagStartSequence());
    }

    @Test
    public void shouldGetVersion() {
        assertEquals("0.2 Alpha", this.CUT.getVersion());
    }

    @Test
    public void shouldGetPaths() {
        ArrayList<String> extensions = this.CUT.getPaths().get("./");
        assertEquals("./", this.CUT.getPaths().keySet().toArray()[0]);
        assertEquals("java", extensions.get(0));
    }
}