package tyre.commons.configuration;


import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class FileConfigurationProviderTest {
    FileConfigurationProvider CUT = null;

    @Before
    public void initEnvironment() {
        CUT = new FileConfigurationProvider();
    }

    @Test
    public void shouldSaveAndLoad() throws Exception {
        // tests load as well as save
        final DefaultConfigProvider defaultConfig = new DefaultConfigProvider();
        final String filename = "config.ini";
        CUT.getConfigFromOtherProvider(defaultConfig);
        CUT.save(filename);
        FileConfigurationProvider other = new FileConfigurationProvider(filename, true);
        String path = other.getPaths().entrySet().iterator().next().getKey();
        String extension = other.getPaths().entrySet().iterator().next().getValue().get(0);
        assertEquals(defaultConfig.getLocalCacheFilename(), other.getLocalCacheFilename());
        assertEquals(defaultConfig.getTagStartSequence(), other.getTagStartSequence());
        assertEquals("./", path);
        assertEquals("java", extension);
    }

    @Test
    //FIXME: This test is too long
    //FIXME: 
    public void shouldGetConfigFromOtherProviderTest() {
        final String expectedLocalCacheFilename = "expLocCache";
        final String expectedTagStartSequence = "#";
        final String expectedPath0 = "\\expected\\path\\0";
        final String expectedExtension0 = "java";
        final String expectedPath1 = "\\expected\\path\\1";
        final String expectedExtension1 = "c";

        final ConfigurationProvider mockProvider = new ConfigurationProvider() {
            public String getLocalCacheFilename() {
                return expectedLocalCacheFilename;
            }

            public String getTagStartSequence() {
                return expectedTagStartSequence;
            }

            public String getVersion() {
                return null; // unused in tested method
            }

            public Map<String, ArrayList<String>> getPaths() {
                Map<String, ArrayList<String>> paths = new HashMap<String, ArrayList<String>>();
                ArrayList<String> extensions0 = new ArrayList<String>();
                extensions0.add(expectedExtension0);
                ArrayList<String> extensions1 = new ArrayList<String>();
                extensions1.add(expectedExtension1);
                paths.put(expectedPath0, extensions0);
                paths.put(expectedPath1, extensions1);
                return paths;
            }

            @Override
            public String getOtherValue(String key) {
                return null;
            }

            @Override
            public Map<String, String> getOtherValues() {
                return null;
            }
        };

        CUT.getConfigFromOtherProvider(mockProvider);

        Iterator<Map.Entry<String, ArrayList<String>>> iterator = CUT.getPaths().entrySet().iterator();
        Map.Entry<String, ArrayList<String>> entry2 = iterator.next();
        Map.Entry<String, ArrayList<String>> entry1 = iterator.next();

        String path0 = entry1.getKey();
        String ext0 = entry1.getValue().get(0);

        String path1 = entry2.getKey();
        String ext1 = entry2.getValue().get(0);

        assertEquals(expectedLocalCacheFilename, CUT.getLocalCacheFilename());
        assertEquals(expectedTagStartSequence, CUT.getTagStartSequence());
        assertEquals(expectedPath0, path0);
        assertEquals(expectedExtension0, ext0);
        assertEquals(expectedPath1, path1);
        assertEquals(expectedExtension1, ext1);
    }

    @Test
    public void shouldNotLoad() throws Exception {
        final DefaultConfigProvider defaultConfig = new DefaultConfigProvider();
        final String filename = "this_file_should_not_exist";
        CUT.load(filename);
        assertFalse(CUT.isFileLoaded());
    }
}
