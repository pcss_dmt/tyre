package tyre.commons.externalData.provider;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;
import tyre.commons.parser.FileParser;
import tyre.commons.requirements.management.ProviderInternalErrorException;
import tyre.commons.requirements.model.Requirement;
import tyre.commons.utils.RestDriver;

import javax.ws.rs.core.UriBuilder;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConfluenceProviderTest {
    private final String projectPath = System.getProperty("user.dir");
    private final String dirPath = FilenameUtils.separatorsToSystem(projectPath + "/src/test/resources/filesToTestProviders/");
    private final String baseUrl = "https://confluence.man.poznan.pl/community";
    private final String uriString = baseUrl + "/rest/api/content/47808855/child/page";
    private List<Requirement> requirementList;

    @Before
    public void init() {
        final String version = FileParser.NO_VERSION;
        requirementList = new ArrayList<Requirement>();
        requirementList.add(new Requirement("US1", version, "Generating HTML Report", this.baseUrl + "/x/boLZAg"));
        requirementList.add(new Requirement("US2", version, "Implementing support for non-Java", this.baseUrl + "/x/cYLZAg"));
        requirementList.add(new Requirement("US3", version, "Defining Requirements", this.baseUrl + "/x/c4LZAg"));
        requirementList.add(new Requirement("US4", version, "Checking Requirement implementation status", this.baseUrl + "/x/dYLZAg"));
        requirementList.add(new Requirement("US5", version, "Checking files implementing Requirement", this.baseUrl + "/x/d4LZAg"));
        requirementList.add(new Requirement("US6", version, "Marking code implementing Requirement", this.baseUrl + "/x/eYLZAg"));
        requirementList.add(new Requirement("US7", version, "Searching Requirements", this.baseUrl + "/x/e4LZAg"));
        requirementList.add(new Requirement("US8", version, "Checking code usage", this.baseUrl + "/x/fYLZAg"));
        requirementList.add(new Requirement("US9", version, "Monitoring requirements", this.baseUrl + "/x/m4LZAg"));
        requirementList.add(new Requirement("US10", version, "Modifying requirements", this.baseUrl + "/x/nYLZAg"));
        requirementList.add(new Requirement("US11", version, "Identifying changes in Requirements", this.baseUrl + "/x/eIPZAg"));
    }

    @Test
    public void localTestConfluenceProvider() throws ProviderInternalErrorException, IOException, URISyntaxException {
        final String data_1 = FileUtils.readFileToString(new File(this.dirPath + "confluence_1.txt"));
        final String data_2 = FileUtils.readFileToString(new File(this.dirPath + "confluence_2.txt"));
        final String data_3 = FileUtils.readFileToString(new File(this.dirPath + "confluence_3.txt"));

        final URI uri_1 = new URI(this.uriString);
        final URI uri_2 = new URI(baseUrl + "/rest/api/content/47809407/child/page");
        final UriBuilder uriBuilder = UriBuilder.fromUri(uri_1);
        final RestDriver mockRestDriver = mock(RestDriver.class);

        when(mockRestDriver.getResponse(any(URI.class))).thenReturn(data_3);
        when(mockRestDriver.getResponse(uri_1)).thenReturn(data_1);
        when(mockRestDriver.getResponse(uri_2)).thenReturn(data_2);
        when(mockRestDriver.getUri()).thenReturn(uri_1);

        final ConfluenceProvider confluenceProvider = new ConfluenceProvider(mockRestDriver);

        assertTrue(confluenceProvider.getAllRequirements().containsAll(requirementList));
        assertTrue(requirementList.containsAll(confluenceProvider.getAllRequirements()));
    }

    @Test
    public void testConfluenceProviderOnline() throws ProviderInternalErrorException, IOException {
        final RestDriver driver = new RestDriver(this.uriString, null);
        final ConfluenceProvider confluenceProvider = new ConfluenceProvider(driver);

        assertTrue(confluenceProvider.getAllRequirements().containsAll(requirementList));
        assertTrue(requirementList.containsAll(confluenceProvider.getAllRequirements()));
    }

    @Test
    public void testConfluenceProviderOnlineByParts() throws ProviderInternalErrorException, IOException {
        final RestDriver driver = new RestDriver(this.uriString + "?limit=3", null);
        final ConfluenceProvider confluenceProvider = new ConfluenceProvider(driver);

        assertTrue(confluenceProvider.getAllRequirements().containsAll(requirementList));
        assertTrue(requirementList.containsAll(confluenceProvider.getAllRequirements()));
    }

    //    @Test
    public void testConfluenceProviderOnlineWithAuth() throws ProviderInternalErrorException, IOException {
        final RestDriver driver = new RestDriver(this.uriString, null);
        final ConfluenceProvider confluenceProvider = new ConfluenceProvider(driver);

        assertTrue(confluenceProvider.getAllRequirements().containsAll(requirementList));
        assertTrue(requirementList.containsAll(confluenceProvider.getAllRequirements()));
    }
}