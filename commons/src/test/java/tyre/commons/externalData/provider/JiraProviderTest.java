package tyre.commons.externalData.provider;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;
import tyre.commons.parser.FileParser;
import tyre.commons.requirements.management.ProviderInternalErrorException;
import tyre.commons.requirements.model.Requirement;
import tyre.commons.utils.RestDriver;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JiraProviderTest {

    private final String projectPath = System.getProperty("user.dir");
    private final String dirPath = FilenameUtils.separatorsToSystem(projectPath + "/src/test/resources/filesToTestProviders/");
    private final String baseUrl = "https://jira.man.poznan.pl/jira";
    private final String uriString = baseUrl + "/rest/api/2/search?jql=project=REQ&fields=summary";
    private List<Requirement> requirementList;

    @Before
    public void init() {
        final String browsePath = this.baseUrl + "/browse";
        final String version = FileParser.NO_VERSION;
        requirementList = new ArrayList<Requirement>();
        requirementList.add(new Requirement("REQ-142", version, "Summary information", browsePath + "/REQ-142"));
        requirementList.add(new Requirement("REQ-141", version, "Make console output more clear/readable (command find and cache-find)", browsePath + "/REQ-141"));
        requirementList.add(new Requirement("REQ-140", version, "Use TYRE tool in TYRE project", browsePath + "/REQ-140"));
        requirementList.add(new Requirement("REQ-139", version, "References to JIRA", browsePath + "/REQ-139"));
        requirementList.add(new Requirement("REQ-138", version, "Rename commands operating on local-cache - use prefix 'cache-' (e.g. cache-find, cache-create).", browsePath + "/REQ-138"));
        requirementList.add(new Requirement("REQ-137", version, "Tracking how changes of requirements definition were reflected in changes in the code", browsePath + "/REQ-137"));
        requirementList.add(new Requirement("REQ-136", version, "Parse: print annotated code elements (or code snippets)", browsePath + "/REQ-136"));
        requirementList.add(new Requirement("REQ-135", version, "Clickable references to files in console output (Intellij IDEA, Eclipse)", browsePath + "/REQ-135"));
        requirementList.add(new Requirement("REQ-134", version, "Can't use titles longer than 1 word when puting a new requirement into local cache", browsePath + "/REQ-134"));
        requirementList.add(new Requirement("REQ-133", version, "HTML report: reference to remote file", browsePath + "/REQ-133"));
    }

    @Test
    public void localTestJiraProvider() throws IOException, ProviderInternalErrorException, URISyntaxException {
        final String data_1 = FileUtils.readFileToString(new File(this.dirPath + "jira_1.txt"));
        final RestDriver mockRestDriver = mock(RestDriver.class);

        final URI uri_1 = new URI(this.uriString);

        when(mockRestDriver.getResponse(any(URI.class))).thenReturn(data_1);
        when(mockRestDriver.getUri()).thenReturn(uri_1);

        JiraProvider jiraProvider = new JiraProvider(mockRestDriver);

        assertTrue(requirementList.containsAll(jiraProvider.getAllRequirements()));
        assertTrue(jiraProvider.getAllRequirements().containsAll(requirementList));
    }
}
