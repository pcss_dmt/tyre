package tyre.commons.externalData.provider;

import org.junit.Before;
import org.junit.Test;
import tyre.commons.externalData.provider.CommandlineProvider;
import tyre.commons.requirements.management.ProviderInternalErrorException;
import tyre.commons.requirements.model.Requirement;

import java.util.List;

import static org.junit.Assert.*;

public class CommandlineProviderTest {

    private CommandlineProvider CUT = null;

    @Before
    public void initialize() {
        CUT = new CommandlineProvider();
    }

    @Test
    public void shouldAddRequirement() throws ProviderInternalErrorException {
        final String id = "0";
        final Requirement testRequirement = new Requirement(
                id,
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        CUT.addRequirement(testRequirement);
        // also tests positive test case for getById method
        assertEquals(testRequirement,CUT.getById(id).get(0));
    }

    @Test
    public void shouldNotAddRequirement() throws ProviderInternalErrorException {
        final Requirement testRequirement = new Requirement(
                "0",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        CUT.addRequirement(testRequirement);
        assertFalse(CUT.addRequirement(testRequirement));
    }

    @Test
    public void shouldNotGetById() throws ProviderInternalErrorException {
        final String id = "0";
        // nothing is present in the provider
        final Requirement testRequirement = new Requirement(
                "1",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        CUT.addRequirement(testRequirement);
        // no requirement of provided id is present in the provider
        assertTrue(CUT.getById(id).isEmpty());
    }

    @Test
    public void shouldGetAllRequirements() throws ProviderInternalErrorException {
        final Requirement testRequirement0 = new Requirement(
                "0",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        final Requirement testRequirement1 = new Requirement(
                "1",
                "1.0.1",
                "Requirement1",
                "http://www.example.com/Requirement/1"
        );
        final Requirement testRequirement2 = new Requirement(
                "2",
                "1.0.2",
                "Requirement2",
                "http://www.example.com/Requirement/2"
        );
        CUT.addRequirement(testRequirement0);
        CUT.addRequirement(testRequirement1);
        CUT.addRequirement(testRequirement2);
        List<Requirement> requirements = CUT.getAllRequirements();
        assertEquals(testRequirement0, requirements.get(0));
        assertEquals(testRequirement1, requirements.get(1));
        assertEquals(testRequirement2, requirements.get(2));
    }

    @Test
    public void shouldNotGetAllRequirements() throws ProviderInternalErrorException {
        // CUT should be empty
        List<Requirement> requirements = CUT.getAllRequirements();
        assertTrue(requirements.isEmpty());
    }

}