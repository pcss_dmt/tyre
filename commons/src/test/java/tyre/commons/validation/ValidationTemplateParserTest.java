package tyre.commons.validation;

import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;
import tyre.commons.parser.FileParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class ValidationTemplateParserTest {

    private ValidationTemplateParser CUT;
    private final String projectPath = System.getProperty("user.dir");
    private final String dirPath = FilenameUtils.separatorsToSystem(projectPath + "/src/test/resources/filesToTestValidationTemplateParser/");
    private final String file1 = dirPath + "template1.txt";
    private final String file2 = dirPath + "template2.txt";
    private final String file3 = dirPath + "template3.txt";

    @Before
    public void setUp() throws Exception {
        CUT = new ValidationTemplateParser();
    }

    @Test
    public void testBasicParsing() {
        Map<String, ArrayList<String>> output = CUT.parse(file1);

        boolean test = false;
        String key = "US1:" + FileParser.NO_VERSION;

        if(output.containsKey(key)) {
            List<String> files = output.get(key);
            if(files.size() == 1 && files.get(0).equals("Operation.java")) {
                test = true;
            }
        }

        assertTrue(test);
    }

    @Test
    public void testMultipleEntries() {
        Map<String, ArrayList<String>> output = CUT.parse(file2);

        boolean test = false;
        String key = "US1:1";

        if(output.containsKey(key)) {
            List<String> files = output.get(key);
            if(files.size() == 2 && files.get(0).equals("SomeTestFile.java")
                    && files.get(1).equals("SomeOtherTestFile.java")) {
                test = true;
            }
        }

        assertTrue(test);
    }

    @Test
    public void testMultipleRequirements() {
        Map<String, ArrayList<String>> output = CUT.parse(file2);

        boolean test = false;
        String key1 = "US1:1";
        String key2 = "US2:" + FileParser.NO_VERSION;

        if(output.containsKey(key1) && output.containsKey(key2)) {
            List<String> files1 = output.get(key1);
            List<String> files2 = output.get(key2);
            if(files1.size() == 2 && files1.get(0).equals("SomeTestFile.java")
                    && files1.get(1).equals("SomeOtherTestFile.java")
                    && files2.size() == 2 && files2.get(0).equals("Test.java")
                    && files2.get(1).equals("AnotherTest.java")) {
                test = true;
            }
        }

        assertTrue(test);
    }

    @Test
    public void testVersionParsing() {
        Map<String, ArrayList<String>> output = CUT.parse(file3);

        boolean test = false;
        String key = "US1:1";

        if(output.containsKey(key)) {
            List<String> files = output.get(key);
            if(files.size() == 1 && files.get(0).equals("SomeTestFile.java")) {
                test = true;
            }
        }

        assertTrue(test);
    }
}