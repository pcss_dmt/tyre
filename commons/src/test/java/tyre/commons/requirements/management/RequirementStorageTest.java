package tyre.commons.requirements.management;

import org.junit.*;
import tyre.commons.requirements.model.Requirement;

import java.util.List;

public class RequirementStorageTest {
    private RequirementStorage CUT = null;

    @Before
    public void initEnvironment() {
        CUT = new RequirementStorage();
        CUT.clear();
    }

    @Test
    public void getAllRequirementsTest() {
        // declare test requirements data
        final Requirement testRequirement0 = new Requirement(
                "0",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        final Requirement testRequirement1 = new Requirement(
                "1",
                "1.0.1",
                "Requirement1",
                "http://www.example.com/Requirement/1"
        );
        final Requirement testRequirement2 = new Requirement(
                "1",
                "1.0.2",
                "Requirement2",
                "http://www.example.com/Requirement/2"
        );
        // add test data
        CUT.addOrUpdate(testRequirement0, "dummy");
        CUT.addOrUpdate(testRequirement1, "dummy");
        CUT.addOrUpdate(testRequirement2, "dummy");
        List<Requirement> requirementList = CUT.getAllRequirements();
        // verify if there are 3 requirements on CUTs list
        Assert.assertEquals(3,requirementList.size());
        // verify that references of requirement on the list and added requirement are equal
        Assert.assertSame(testRequirement0,requirementList.get(0));
        Assert.assertSame(testRequirement1,requirementList.get(1));
        Assert.assertSame(testRequirement2,requirementList.get(2));
    }

    @Test
    public void shouldGetByIdTest() {
        // declare test requirements data
        final String searchedId = "1";
        final Requirement testRequirement0 = new Requirement(
                "0",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        final Requirement testRequirement1 = new Requirement(
                searchedId,
                "1.0.1",
                "Requirement1",
                "http://www.example.com/Requirement/1"
        );
        final Requirement testRequirement2 = new Requirement(
                "1",
                "1.0.2",
                "Requirement2",
                "http://www.example.com/Requirement/2"
        );
        // add test data
        CUT.addOrUpdate(testRequirement0, "dummy");
        CUT.addOrUpdate(testRequirement1, "dummy");
        CUT.addOrUpdate(testRequirement2, "dummy");
        Requirement returned = CUT.getById(searchedId).get(0);
        Assert.assertSame(testRequirement1, returned);
    }

    @Test
    public void shouldNotGetByIdTest() {
        // declare test requirements data
        final String searchedId = "1";
        // verify if empty CUT is really empty
        Assert.assertTrue(CUT.getById(searchedId).isEmpty());
    }

    @Test
    public void shouldSynchronizeTest() throws Exception {
        // Note: this test method is also positive test case for load() functionality

        // declare test requirements data
        final Requirement testRequirement0 = new Requirement(
                "0",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        final Requirement testRequirement1 = new Requirement(
                "1",
                "1.0.1",
                "Requirement1",
                "http://www.example.com/Requirement/1"
        );
        final Requirement testRequirement2 = new Requirement(
                "1",
                "1.0.2",
                "Requirement2",
                "http://www.example.com/Requirement/2"
        );
        // add test data
        CUT.addOrUpdate(testRequirement0, "dummy");
        CUT.addOrUpdate(testRequirement1, "dummy");
        CUT.addOrUpdate(testRequirement2, "dummy");
        RequirementStorage otherCUT = new RequirementStorage();
        otherCUT.load();
        List<Requirement> otherList = otherCUT.getAllRequirements();
        Assert.assertEquals("synchronizeTest()::list_size", 3, otherList.size());
        Assert.assertEquals("synchronizeTest()::req0", testRequirement0, otherList.get(0));
        Assert.assertEquals("synchronizeTest()::req1", testRequirement1, otherList.get(1));
        Assert.assertEquals("synchronizeTest()::req2", testRequirement2, otherList.get(2));
    }

    @Test
    public void shouldUpdateRequirementTest() {
        final String newTitle = "Requirement0p";
        final Requirement testRequirement0 = new Requirement(
                "0",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        final Requirement updatedRequirement = new Requirement(
                "0",
                "1.0.0",
                newTitle,
                "http://www.example.com/Requirement/0"
        );
        CUT.addOrUpdate(testRequirement0, "dummy");
        boolean trueExpected = CUT.updateRequirement(
                updatedRequirement, "dummy") && newTitle.equals(CUT.getAllRequirements().get(0).getTitle());
        Assert.assertTrue(trueExpected);
    }


    @Test
    public void shouldNotUpdateRequirementTest() {
        final Requirement testRequirement0 = new Requirement(
                "0",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        CUT.addOrUpdate(testRequirement0, "dummy");
        Assert.assertEquals(testRequirement0, CUT.getAllRequirements().get(0));
        // below operation should fail as there is no requirement identified by id "1" and "1.0.1"
        boolean falseExpected = CUT.updateRequirement(new Requirement("1","1.0.1","ABC","CDF"), "dummy");
        Assert.assertFalse(falseExpected);
    }


    @Test
    public void shouldAddTest() {
        final Requirement testRequirement0 = new Requirement(
                "0",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        boolean trueExpected = CUT.addOrUpdate(
                testRequirement0, "dummy") && CUT.getAllRequirements().get(0).equals(testRequirement0);
        Assert.assertTrue(trueExpected);
    }

}
