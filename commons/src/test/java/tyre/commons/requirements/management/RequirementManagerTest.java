package tyre.commons.requirements.management;

import org.junit.Before;
import org.junit.Test;
import tyre.commons.requirements.model.Requirement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertSame;

public class RequirementManagerTest {
    private class TestProvider implements RequirementProvider {
        public boolean getAllRequirementsInvoked;
        public boolean getByIdInvoked;
        public boolean getProviderTypeInvoked;

        private Requirement outputReq;

        public TestProvider(Requirement outputReq) {
            getAllRequirementsInvoked = false;
            getByIdInvoked = false;
            getProviderTypeInvoked = false;
            this.outputReq = outputReq;
        }

        public List<Requirement> getAllRequirements() {
            getAllRequirementsInvoked = true;
            return new ArrayList<Requirement>(Arrays.asList(new Requirement[]{outputReq}));
        }

        public List<Requirement> getById(String id) {
            getByIdInvoked = true;
            return new ArrayList<Requirement>(Arrays.asList(new Requirement[]{outputReq}));
        }

        public String getProviderType() {
            getProviderTypeInvoked = true;
            return "testProvider";
        }
    }

    private RequirementManager CUT = null;

    @Before
    public void environmentInitialization() {
        CUT = new RequirementManager();
        CUT.getRequirementStorage().clear();
    }

    @Test
    public void shouldAddProviderTest() {
        // Note: RequirementManager.getProviders() is tested here as well
        final TestProvider provider = new TestProvider(null);
        assertTrue(this.CUT.addProvider(provider));
        assertEquals(provider, this.CUT.getProviders().get(0));
    }

    @Test
    public void shouldNotAddProviderTest0() {
        assertFalse(this.CUT.addProvider(null));
    }

    @Test
    public void shouldNotAddProviderTest1() {
        final TestProvider provider = new TestProvider(null);
        this.CUT.addProvider(provider);
        assertFalse(this.CUT.addProvider(provider));
    }

    @Test
    public void getAllRequirementsTest() {
        // declare test requirements data
        final Requirement testRequirement0 = new Requirement(
                "0",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        final Requirement testRequirement1 = new Requirement(
                "1",
                "1.0.1",
                "Requirement1",
                "http://www.example.com/Requirement/1"
        );
        final Requirement testRequirement2 = new Requirement(
                "2",
                "1.0.2",
                "Requirement2",
                "http://www.example.com/Requirement/2"
        );

        TestProvider testProvider0 = new TestProvider(testRequirement0);
        TestProvider testProvider1 = new TestProvider(testRequirement1);
        TestProvider testProvider2 = new TestProvider(testRequirement2);
        // add test data
        CUT.addProvider(testProvider0);
        CUT.addProvider(testProvider1);
        CUT.addProvider(testProvider2);
        List<Requirement> requirementList = CUT.getAllRequirements();
        // verify if there are 3 requirements on CUTs list
        assertEquals(3, requirementList.size());
        // verify that references of requirement on the list and added requirement are equal
        assertSame(testRequirement0, requirementList.get(0));
        assertSame(testRequirement1, requirementList.get(1));
        assertSame(testRequirement2, requirementList.get(2));
        assertTrue(testProvider0.getAllRequirementsInvoked);
        assertTrue(testProvider1.getAllRequirementsInvoked);
        assertTrue(testProvider2.getAllRequirementsInvoked);
    }

    @Test
    public void shouldGetByIdTest() {
        // declare test requirements data
        final Requirement testRequirement0 = new Requirement(
                "0",
                "1.0.0",
                "Requirement0",
                "http://www.example.com/Requirement/0"
        );
        final Requirement testRequirement1 = new Requirement(
                "1",
                "1.0.1",
                "Requirement1",
                "http://www.example.com/Requirement/1"
        );
        final Requirement testRequirement2 = new Requirement(
                "2",
                "1.0.2",
                "Requirement2",
                "http://www.example.com/Requirement/2"
        );

        TestProvider testProvider0 = new TestProvider(testRequirement0);
        TestProvider testProvider1 = new TestProvider(testRequirement1);
        TestProvider testProvider2 = new TestProvider(testRequirement2);
        // add test data
        CUT.addProvider(testProvider0);
        CUT.addProvider(testProvider1);
        CUT.addProvider(testProvider2);
        List<Requirement> requirementList = CUT.getById(testRequirement1.getId());
        // verify if there is one requirement on CUTs list
        assertEquals(1, requirementList.size());
        // verify that references of requirement on the list and added requirement are equal
        assertSame(testRequirement1, requirementList.get(0));
        assertTrue(testProvider0.getByIdInvoked);
        assertTrue(testProvider1.getByIdInvoked);
        assertTrue(testProvider2.getByIdInvoked);
    }

    @Test
    public void shouldNotGetByIdTest() {
        // declare test requirements data
        final String searchedId = "1";
        // verify if empty CUT is really empty
        assertTrue(CUT.getById(searchedId).isEmpty());
    }


}