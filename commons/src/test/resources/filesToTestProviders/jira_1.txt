{
    "expand":"schema,names",
    "startAt":0,
    "maxResults":50,
    "total":10,
    "issues":[
        {
            "expand":"editmeta,renderedFields,transitions,changelog,operations",
            "id":"88108",
            "self":"https://jira.man.poznan.pl/jira/rest/api/2/issue/88108",
            "key":"REQ-142",
            "fields":{
                "summary":"Summary information"
            }
        },
        {
            "expand":"editmeta,renderedFields,transitions,changelog,operations",
            "id":"88107",
            "self":"https://jira.man.poznan.pl/jira/rest/api/2/issue/88107",
            "key":"REQ-141",
            "fields":{
                "summary":"Make console output more clear/readable (command find and cache-find)"
            }
        },
        {
            "expand":"editmeta,renderedFields,transitions,changelog,operations",
            "id":"88103",
            "self":"https://jira.man.poznan.pl/jira/rest/api/2/issue/88103",
            "key":"REQ-140",
            "fields":{
                "summary":"Use TYRE tool in TYRE project"
            }
        },
        {
            "expand":"editmeta,renderedFields,transitions,changelog,operations",
            "id":"88102",
            "self":"https://jira.man.poznan.pl/jira/rest/api/2/issue/88102",
            "key":"REQ-139",
            "fields":{
                "summary":"References to JIRA"
            }
        },
        {
            "expand":"editmeta,renderedFields,transitions,changelog,operations",
            "id":"88052",
            "self":"https://jira.man.poznan.pl/jira/rest/api/2/issue/88052",
            "key":"REQ-138",
            "fields":{
                "summary":"Rename commands operating on local-cache - use prefix 'cache-' (e.g. cache-find, cache-create)."
            }
        },
        {
            "expand":"editmeta,renderedFields,transitions,changelog,operations",
            "id":"88012",
            "self":"https://jira.man.poznan.pl/jira/rest/api/2/issue/88012",
            "key":"REQ-137",
            "fields":{
                "summary":"Tracking how changes of requirements definition were reflected in changes in the code"
            }
        },
        {
            "expand":"editmeta,renderedFields,transitions,changelog,operations",
            "id":"88010",
            "self":"https://jira.man.poznan.pl/jira/rest/api/2/issue/88010",
            "key":"REQ-136",
            "fields":{
                "summary":"Parse: print annotated code elements (or code snippets)"
            }
        },
        {
            "expand":"editmeta,renderedFields,transitions,changelog,operations",
            "id":"87980",
            "self":"https://jira.man.poznan.pl/jira/rest/api/2/issue/87980",
            "key":"REQ-135",
            "fields":{
                "summary":"Clickable references to files in console output (Intellij IDEA, Eclipse)"
            }
        },
        {
            "expand":"editmeta,renderedFields,transitions,changelog,operations",
            "id":"87978",
            "self":"https://jira.man.poznan.pl/jira/rest/api/2/issue/87978",
            "key":"REQ-134",
            "fields":{
                "summary":"Can't use titles longer than 1 word when puting a new requirement into local cache"
            }
        },
        {
            "expand":"editmeta,renderedFields,transitions,changelog,operations",
            "id":"87929",
            "self":"https://jira.man.poznan.pl/jira/rest/api/2/issue/87929",
            "key":"REQ-133",
            "fields":{
                "summary":"HTML report: reference to remote file"
            }
        }
    ]
}