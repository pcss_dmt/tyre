package tyre.commons.externalData.provider;

import java.util.Map;

/**
 * Interface using to gather config values
 * which provider would need
 */

public interface ProviderConfig {
    Map<String, String> getConfigValues();
}
