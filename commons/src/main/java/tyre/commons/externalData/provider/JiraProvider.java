package tyre.commons.externalData.provider;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import tyre.commons.externalData.model.JiraJsonParser;
import tyre.commons.externalData.model.JiraNavigator;
import tyre.commons.requirements.management.ProviderInternalErrorException;
import tyre.commons.requirements.management.RequirementProvider;
import tyre.commons.requirements.model.Requirement;
import tyre.commons.utils.RestDriver;

import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class provides integration with Jira REST API.
 * Uses Jira REST API to search requirements
 * from particular space.
 */
public final class JiraProvider implements RequirementProvider, ProviderConfig {

    private static final Logger LOGGER = Logger.getLogger(JiraProvider.class.getPackage().getName());
    private static final String exceptionMessage = "Unable to retrieve data from Jira Provider";
    private static final Map<String, String> configValues = createConfigValuesMap();

    private final RestDriver driver;
    private final ObjectMapper mapper;
    private List<Requirement> requirements;

    private static Map<String, String> createConfigValuesMap() {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("jira_url", "Set your URL to Jira");
        aMap.put("jira_login", "Set your login to Jira");
        return Collections.unmodifiableMap(aMap);
    }

    public JiraProvider(final RestDriver driver) throws ProviderInternalErrorException {
        this.driver = driver;
        this.mapper = new ObjectMapper();

        try {
            final SimpleModule simpleModule = new SimpleModule();
            simpleModule.addDeserializer(List.class, new JiraJsonParser(this.driver.getUri()));
            simpleModule.addDeserializer(Set.class, JiraNavigator.getJiraNavigator(this.driver.getUri()));
            mapper.registerModule(simpleModule);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Low-level I/O problem (unexpected end-of-input, network error)", e);
            throw new ProviderInternalErrorException(exceptionMessage, e);
        }
        this.requirements = this.process();
    }

    private List<Requirement> getJsonSet() throws IOException, ProviderInternalErrorException {
        return this.getJsonSet(null);
    }

    private List<Requirement> getJsonSet(final URI uri) throws IOException, ProviderInternalErrorException {
        final List<Requirement> resultsList = new ArrayList<Requirement>();
        final URI nextResult = uri == null ? this.driver.getUri() : uri;
        final String responseToParse = this.driver.getResponse(nextResult);
        final List<Requirement> list = mapper.readValue(responseToParse,
                mapper.getTypeFactory().constructCollectionType(List.class, Requirement.class));
        final Set<URI> uriSet = mapper.readValue(responseToParse,
                mapper.getTypeFactory().constructCollectionType(Set.class, URI.class));

        resultsList.addAll(list);
        for (final URI uriToExplore : uriSet)
            resultsList.addAll(this.getJsonSet(uriToExplore));

        return resultsList;
    }

    private List<Requirement> process() throws ProviderInternalErrorException {
        try {
            return this.getJsonSet();

        } catch (final JsonMappingException e) {
            LOGGER.log(Level.SEVERE, "Input JSON structure does not match structure expected for result type (or has other mismatch issues)", e);
            throw new ProviderInternalErrorException(exceptionMessage, e);
        } catch (final JsonParseException e) {
            LOGGER.log(Level.SEVERE, "Input contains invalid content of type JsonParser supports (JSON for default case)", e);
            throw new ProviderInternalErrorException(exceptionMessage, e);
        } catch (final IOException e) {
            LOGGER.log(Level.SEVERE, "Low-level I/O problem (unexpected end-of-input, network error)", e);
            throw new ProviderInternalErrorException(exceptionMessage, e);
        }
    }

    @Override
    public List<Requirement> getAllRequirements() {
        return requirements;
    }

    @Override
    public List<Requirement> getById(String id) {
        List<Requirement> toReturn = new ArrayList<Requirement>();
        for (final Requirement requirement : requirements)
            if (requirement.getId().equals(id))
                toReturn.add(requirement);
        return toReturn;
    }

    @Override
    public String getProviderType() {
        return "jira";
    }

    @Override
    public Map<String, String> getConfigValues() {
        return configValues;
    }
}












