package tyre.commons.externalData.provider;

import tyre.commons.requirements.management.RequirementProvider;
import tyre.commons.requirements.model.Requirement;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Provider that acts as a commandline requirements source.
 */
public class CommandlineProvider implements RequirementProvider {

    private static final Logger logger = Logger.getLogger(CommandlineProvider.class.
            getPackage().getName());

    public static final String PROVIDER_TYPE = "commandline";

    private List<Requirement> requirements;

    private boolean containsRequirement(String id, String version) {
        for (Requirement requirement : requirements) {
            if (requirement.getId().equals(id) && requirement.getVersion().equals(version)) {
                return true;
            }
        }
        return false;
    }

    public CommandlineProvider() {
        requirements = new ArrayList<Requirement>();
    }

    /**
     * If provided requirement is not present or null, it is added.
     *
     * @param requirement requirement that will be added.
     * @return boolean indicating if requirement was successfully added.
     */
    public boolean addRequirement(Requirement requirement) {
        if (!containsRequirement(requirement.getId(), requirement.getVersion())) {
            return requirements.add(requirement);
        }
        return false;
    }

    @Override
    public List<Requirement> getAllRequirements() {
        return new ArrayList<Requirement>(requirements);
    }

    @Override
    public List<Requirement> getById(String id) {
        List<Requirement> output = new ArrayList<Requirement>();
        for (Requirement requirement : requirements) {
            if (requirement.getId().equals(id)) {
                output.add(requirement);
            }
        }
        return output;
    }

    @Override
    public String getProviderType() {
        return "commandline";
    }
}
