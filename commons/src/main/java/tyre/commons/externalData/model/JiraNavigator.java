package tyre.commons.externalData.model;


import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

public class JiraNavigator extends JsonDeserializer<Set<URI>> {

    private final UriBuilder uriBuilder;

    private JiraNavigator(final URI uri) {
        this.uriBuilder = UriBuilder.fromUri(uri);
    }

    public static JiraNavigator getJiraNavigator(final URI uri) {
        return new JiraNavigator(uri);
    }

    @Override
    public Set<URI> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        final Set<URI> toReturn = new HashSet<URI>();
        final JsonNode root = jsonParser.getCodec().readTree(jsonParser);
        final URI uri = this.getNextUri(root);

        if (uri != null)
            toReturn.add(uri);

        return toReturn;
    }

    private URI getNextUri(final JsonNode node) {
        // computes if there are any jsons to download
        int startAt = node.path("startAt").intValue();
        int maxResults = node.path("maxResults").intValue();
        int total = node.path("total").intValue();
        int nextStart = startAt + maxResults;
        int size = total - (nextStart);
        // if yes then return proper URI, if not return null
        return size > 0 ? this.uriBuilder.replaceQueryParam("startAt", nextStart).build() : null;
    }
}
