package tyre.commons.externalData.model;


import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

public class ConfluenceNavigator extends JsonDeserializer<Set<URI>> {

    private final static String PATHBASE = "/community";
    private final UriBuilder uriBuilder;
    private final boolean recursive;

    private ConfluenceNavigator(final URI uri, final boolean recursive) {
        this.uriBuilder = UriBuilder.fromUri(uri);
        this.recursive = recursive;
    }

    public static ConfluenceNavigator getRecursiveConfluenceNavigator(final URI uri) {
        return new ConfluenceNavigator(uri, true);
    }

    public static ConfluenceNavigator getNonRecursiveConfluenceNavigator(final URI uri) {
        return new ConfluenceNavigator(uri, false);
    }

    @Override
    public Set<URI> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        final Set<URI> pagesToExplore = new HashSet<URI>();
        final JsonNode root = jsonParser.getCodec().readTree(jsonParser);
        final JsonNode jsonArray = root.path("results");
        final JsonNode node = root.path("_links").path("next");
        final URI toInsert = this.getNextUri(node);

        if (toInsert != null)
            pagesToExplore.add(toInsert);

        for (final JsonNode singleNode : jsonArray) {
            if (this.recursive && !singleNode.path("title").textValue().matches("(.*):(.*)")) {
                final String path = PATHBASE + singleNode.path("_expandable").path("children").asText() + "/page";
                final URI toExplore = this.uriBuilder.replacePath(path).replaceQuery(null).build();
                pagesToExplore.add(toExplore);
            }
        }
        return pagesToExplore;
    }

    private URI getNextUri(final JsonNode node) {
        final URI uri = UriBuilder.fromUri(PATHBASE + node.textValue()).build();
        return node.isMissingNode() ? null : this.uriBuilder.replacePath(uri.getPath()).replaceQuery(uri.getQuery()).build();
    }
}