package tyre.commons.externalData.model;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import tyre.commons.parser.FileParser;
import tyre.commons.requirements.model.Requirement;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class JiraJsonParser extends JsonDeserializer<List<Requirement>> {

    private final static String PATHBASE = "/jira";
    private final UriBuilder locationUriBuilder;

    public JiraJsonParser(final URI uri) throws IOException {
        this.locationUriBuilder = UriBuilder.fromUri(uri);
    }

    private String getId(final JsonNode node) {
        return node.path("key").textValue().trim();
    }

    private String getVersion(final JsonNode node) {
        return FileParser.NO_VERSION;
    }

    private String getTitle(final JsonNode node) {
        return node.path("fields").path("summary").textValue();
    }

    private String getLocation(final JsonNode node) {
        return this.locationUriBuilder.replacePath(PATHBASE + "/browse/" + getId(node)).replaceQuery(null).toString();
    }

    @Override
    public List<Requirement> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        final List<Requirement> toReturn = new ArrayList<Requirement>();
        final JsonNode root = jsonParser.getCodec().readTree(jsonParser);
        final JsonNode jsonArray = root.path("issues");

        for (final JsonNode singleNode : jsonArray) {
            toReturn.add(new Requirement(getId(singleNode), getVersion(singleNode),
                    getTitle(singleNode), getLocation(singleNode)));
        }
        return toReturn;
    }
}
