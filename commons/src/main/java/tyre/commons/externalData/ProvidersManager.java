package tyre.commons.externalData;

import tyre.commons.configuration.ConfigurationManager;
import tyre.commons.externalData.provider.CommandlineProvider;
import tyre.commons.externalData.provider.ConfluenceProvider;
import tyre.commons.externalData.provider.JiraProvider;
import tyre.commons.requirements.management.ProviderInternalErrorException;
import tyre.commons.requirements.management.RequirementProvider;
import tyre.commons.ui.LoginCredentials;
import tyre.commons.ui.UIMessagePrompt;
import tyre.commons.utils.RestDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProvidersManager {

    private static final Logger LOGGER = Logger.getLogger(ProvidersManager.class.getPackage().getName());
    private final UIMessagePrompt messagePrompt;
    private final ConfigurationManager config;
    private final List<RequirementProvider> providers;

    private CommandlineProvider commandlineProvider;
    private ConfluenceProvider confluenceProvider;
    private JiraProvider jiraProvider;

    public ProvidersManager(final UIMessagePrompt prompt) {

        this.messagePrompt = prompt;
        this.config = ConfigurationManager.getInstance();
        this.providers = new ArrayList<RequirementProvider>();

        this.commandlineProvider = this.addCommandlineProvider();
        try {
            this.confluenceProvider = this.addConfluenceProvider();
        } catch (ProviderInternalErrorException e) {
            LOGGER.log(Level.SEVERE, "Can't create Confluence provider", e);
        }
        try {
            this.jiraProvider = this.addJiraProvider();
        } catch (ProviderInternalErrorException e) {
            LOGGER.log(Level.SEVERE, "Can't create Jira provider", e);
        }
    }

    private CommandlineProvider addCommandlineProvider() {
        CommandlineProvider provider = null;
        provider = new CommandlineProvider();
        this.providers.add(provider);
        return provider;
    }

    private ConfluenceProvider addConfluenceProvider() throws ProviderInternalErrorException {
        LoginCredentials credentials = null;
        if (this.config.getOtherValue("confluence_user") == null ||
                this.config.getOtherValue("confluence_pass") == null) {
            if (this.messagePrompt.showYesNoPrompt("Using account for Confluence access?\n")) {
                credentials = this.messagePrompt.showLoginPrompt("Enter username and password\n");
            }
        } else {
            credentials = new LoginCredentials(this.config.getOtherValue("confluence_user"),
                    this.config.getOtherValue("confluence_pass"));
        }
        RestDriver confluenceDriver = new RestDriver(this.config.getOtherValue("confluence_url"), credentials);
        ConfluenceProvider provider = new ConfluenceProvider(confluenceDriver);
        this.providers.add(provider);
        return provider;
    }

    private JiraProvider addJiraProvider() throws ProviderInternalErrorException {
        LoginCredentials credentials = null;
        if (this.config.getOtherValue("jira_user") == null ||
                this.config.getOtherValue("jira_pass") == null) {
            if (this.messagePrompt.showYesNoPrompt("Using account for Jira access?\n")) {
                credentials = this.messagePrompt.showLoginPrompt("Enter username and password\n");
            }
        } else {
            credentials = new LoginCredentials(this.config.getOtherValue("jira_user"),
                    this.config.getOtherValue("jira_pass"));
        }
        RestDriver jiraDriver = new RestDriver(this.config.getOtherValue("jira_url"), credentials);
        JiraProvider provider = new JiraProvider(jiraDriver);
        this.providers.add(provider);
        return provider;
    }

    public List<RequirementProvider> getProviders() {
        return this.providers;
    }

    public CommandlineProvider getCommandlineProvider() {
        return commandlineProvider;
    }

    public ConfluenceProvider getConfluenceProvider() {
        return confluenceProvider;
    }

    public JiraProvider getJiraProvider() {
        return jiraProvider;
    }

    public void setCommandlineProvider(CommandlineProvider commandlineProvider) {
        if (commandlineProvider != null) {
            this.providers.remove(this.commandlineProvider);
            this.commandlineProvider = commandlineProvider;
            this.providers.add(this.commandlineProvider);
        }
    }

    public void setConfluenceProvider(ConfluenceProvider confluenceProvider) {
        if (confluenceProvider != null) {
            this.providers.remove(this.confluenceProvider);
            this.confluenceProvider = confluenceProvider;
            this.providers.add(this.confluenceProvider);
        }
    }

    public void setJiraProvider(JiraProvider jiraProvider) {
        if (jiraProvider != null) {
            this.providers.remove(this.jiraProvider);
            this.jiraProvider = jiraProvider;
            this.providers.add(this.jiraProvider);
        }
    }
}
