package tyre.commons.configuration;

import java.util.ArrayList;
import java.util.Map;

/**
 * Singleton class that manages fetching all config variables within a system.
 */
//@req(REQ-88)
public class ConfigurationManager implements ConfigurationProvider {

    private static final String DEFAULT_CONFIG_FILENAME = "config.cfg";

    private static ConfigurationManager instance;

    private ConfigurationProvider effectiveConfig = null;

    /**
     * Returns instance of this manager.
     * @return instance of this manager.
     */
    public static ConfigurationManager getInstance() {
        if(instance == null) {
            instance = new ConfigurationManager();
        }
        return instance;
    }

    private ConfigurationManager() {
        this.processInitialization();
    }

    private void processInitialization() {
        FileConfigurationProvider fileProvider = new FileConfigurationProvider(DEFAULT_CONFIG_FILENAME, true);
        if (fileProvider.isFileLoaded()) {
            this.effectiveConfig = fileProvider;

        } else {
            this.effectiveConfig = new DefaultConfigProvider();
        }
    }

    /**
     * Returns name of the local cache file.
     * @return name of the local cache file
     */
    @Override
    public String getLocalCacheFilename() {
        return this.effectiveConfig.getLocalCacheFilename();
    }

    /**
     * Returns sequence of characters that appear in the beginning of tag.
     * @return sequence of characters that appear in the beginning of tag.
     */
    @Override
    public String getTagStartSequence() {
        return this.effectiveConfig.getTagStartSequence();
    }

    /**
     * Returns current version of the tool.
     * @return current version of the tool.
     */
    @Override
    public String getVersion() {
        return this.effectiveConfig.getVersion();
    }

    /**
     * Returns Map containing entries that consist of path-extension list pairs.
     * @return Map containing entries that consist of path-extension list pairs.
     */
    @Override
    public Map<String, ArrayList<String>> getPaths() {
        return this.effectiveConfig.getPaths();
    }

    /**
     * Returns other value identified by key stored by config provider object.
     * @param key key by which the value is identified.
     * @return value identified by key, or null if not present.
     */
    @Override
    public String getOtherValue(String key) {
        return this.effectiveConfig.getOtherValue(key);
    }

    /**
     * Returns map that contains key-value pairs of other configuration variables.
     * @return map that contains key-value pairs of other configuration variables.
     */
    @Override
    public Map<String, String> getOtherValues() {
        return this.effectiveConfig.getOtherValues();
    }
}
