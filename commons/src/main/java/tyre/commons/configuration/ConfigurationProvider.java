package tyre.commons.configuration;

import java.util.ArrayList;
import java.util.Map;

/**
 * Interface that defines methods that are used to fetch configuration variables.
 */
public interface ConfigurationProvider {
    /**
     * Returns name of the local cache file.
     * @return name of the local cache file
     */
    String getLocalCacheFilename();
    /**
     * Returns sequence of characters that appear in the beginning of tag.
     * @return sequence of characters that appear in the beginning of tag.
     */
    String getTagStartSequence();
    /**
     * Returns current version of the tool.
     * @return current version of the tool.
     */
    String getVersion();

    /**
     * Returns Map containing entries that consist of path-extension list pairs.
     * @return Map containing entries that consist of path-extension list pairs.
     */
    Map<String, ArrayList<String>> getPaths();

    /**
     * Returns other value identified by key stored by config provider object.
     * @param key key by which the value is identified.
     * @return value identified by key, or null if not present.
     */
    String getOtherValue(String key);

    /**
     * Returns map that contains key-value pairs of other configuration variables.
     * @return map that contains key-value pairs of other configuration variables.
     */
    Map<String, String> getOtherValues();
}
