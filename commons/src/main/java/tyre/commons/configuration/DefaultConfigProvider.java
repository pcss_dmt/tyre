package tyre.commons.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Class that contains default, hardcoded config.
 */
public class DefaultConfigProvider implements ConfigurationProvider {
    /**
     * Returns name of the local cache file.
     * @return name of the local cache file
     */
    @Override
    public String getLocalCacheFilename() {
        return "localCache";
    }

    //@req(REQ-144)
    /**
     * Returns sequence of characters that appear in the beginning of tag.
     * @return sequence of characters that appear in the beginning of tag.
     */
    @Override
    public String getTagStartSequence() {
        return "@(?i:tyre|requirement|req)";
    }

    /**
     * Returns current version of the tool.
     * @return current version of the tool.
     */
    @Override
    public String getVersion() {
        return "0.2 Alpha";
    }

    /**
     * Returns Map containing entries that consist of path-extension list pairs.
     * @return Map containing entries that consist of path-extension list pairs.
     */
    @Override
    public Map<String, ArrayList<String>> getPaths() {
        HashMap<String, ArrayList<String>> output = new HashMap<String, ArrayList<String>>();
        ArrayList<String> extensions = new ArrayList<String>();
        extensions.add("java");
        output.put("./",extensions);
        return output;
    }

    /**
     * Returns null as DefaultConfigProvider does not contain any other values.
     * @param key key by which the value is identified.
     * @return null.
     */
    @Override
    public String getOtherValue(String key) {
        return null;
    }

    @Override
    public Map<String, String> getOtherValues() {
        return null;
    }
}
