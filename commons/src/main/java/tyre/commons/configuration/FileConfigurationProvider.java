package tyre.commons.configuration;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that provides configuration values stored in the local filesystem. Version parameter is taken directly from
 * DefaultConfigProvider.
 */
public class FileConfigurationProvider implements ConfigurationProvider {

    /**
     * Key of localCacheFilename value.
     */
    public static final String KEY_LOCAL_CACHE_FILENAME = "localCacheFilename";

    /**
     * Key of tagStartSequence value.
     */
    public static final String KEY_TAG_START_SEQUENCE = "tagStartSequence";

    /**
     * Key of path value.
     */
    public static final String KEY_PATHS = "path";

    private String filename;

    private boolean fileLoaded;

    private String localCacheFilename;

    private String tagStartSequence;

    private Map<String, ArrayList<String>> paths;

    private Map<String, String> otherValues;

    private static Logger logger = Logger.getLogger(FileConfigurationProvider.class.getPackage().getName());

    public FileConfigurationProvider() {
        this.filename = "";
        this.fileLoaded = false;
        this.initializeConfigurationValues();
    }

    public FileConfigurationProvider(String filename, boolean autoLoad) {
        this();
        this.filename = filename;
        if (autoLoad) {
            try {
                this.load();
            } catch (Exception e) {
                logger.log(Level.WARNING, "Cannot load config file.", e);
            }
        }
    }

    /**
     * Checks if file of provided filename exists.
     * @param filename name of the file
     * @return boolean indicating if file of provided filename exists.
     */
    private boolean configFileExists(String filename) {
        File file = new File(filename);
        return (file.exists() && !file.isDirectory());
    }

    /**
     * Parses path entry
     * @param value string to be parsed
     */
    private void parsePathAndExtensions(String value) {
        int pos = value.lastIndexOf(':');
        if (pos == -1) {
            return;
        }
        String path = value.substring(0,pos);
        String extensions = value.substring(pos + 1);
        ArrayList<String> extensionList = new ArrayList<String>();
        for(String extension : extensions.split(",")) {
            extensionList.add(extension);
        }
        this.paths.put(path,extensionList);
    }

    /**
     * Parses entry value accordingly to provided key
     * @param key key of the value
     * @param value entry value
     */
    private void parseEntry(String key, String value) {
        if(key.equals(KEY_LOCAL_CACHE_FILENAME)) {
            this.localCacheFilename = value;
        } else if (key.equals(KEY_TAG_START_SEQUENCE)) {
            this.tagStartSequence = value;
        } else if (key.equals(KEY_PATHS)) {
            this.parsePathAndExtensions(value);
        } else {
            this.otherValues.put(key,value);                    /* store as other value */
        }
    }

    /**
     * Parses line provided by reader.
     * @param line provided by reader
     */
    private void parseLine(String line) {
        String regex = "([a-zA-Z0-9\\._\\-]+)[\\s&&[^\\n]]*=[\\s&&[^\\n]]*(.+)$";
        Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(line);
        if(matcher.find()) {
            String key = matcher.group(1);
            String value = matcher.group(2);
            if(key != null && value != null) {
                this.parseEntry(key,value);
            }
        }
    }

    /**
     * Initializes configuration values to blanks.
     */
    private void initializeConfigurationValues() {
        this.localCacheFilename = "";
        this.tagStartSequence = "";
        this.paths = new HashMap<String, ArrayList<String>>();
        this.otherValues = new HashMap<String, String>();
    }

    /**
     * Loads configuration file with filename same as filename field value.
     * @throws Exception exception that can occur during loading file.
     */
    public void load() throws Exception {
        this.load(this.filename);
    }

    /**
     * Loads configuration file with filename same as provided argument.
     * @param filename name of the config file
     * @throws Exception exception that can occur during loading file.
     */
    public void load(String filename) throws Exception {
        this.fileLoaded = false;
        this.initializeConfigurationValues();
        if(this.configFileExists(filename)) {
            FileInputStream ifstream = new FileInputStream(filename);
            BufferedReader reader = new BufferedReader(new InputStreamReader(ifstream));
            String line = reader.readLine();
            while(line != null) {
                this.parseLine(line);
                line = reader.readLine();
            }
            ifstream.close();
            this.fileLoaded = true;
            this.filename = filename;
        }
    }

    /**
     * Saves configuration file with filename same as filename field value.
     * @throws Exception exception that can occur during saving file.
     */
    public void save() throws Exception {
        this.save(this.filename);
    }

    /**
     * Saves configuration file with filename same as provided argument.
     * @param filename name of the config file
     * @throws Exception exception that can occur during saving file.
     */
    public void save(String filename) throws Exception {
        FileOutputStream ofstream = new FileOutputStream(filename);
        PrintWriter writer = new PrintWriter(ofstream);
        writer.println(KEY_LOCAL_CACHE_FILENAME + " = " + this.localCacheFilename);
        writer.println(KEY_TAG_START_SEQUENCE + " = " + this.tagStartSequence);
        for(Map.Entry<String, ArrayList<String>> entry : this.paths.entrySet()) {
            StringBuilder builder = new StringBuilder();
            builder.append(KEY_PATHS);
            builder.append(" = ");
            builder.append(entry.getKey());
            builder.append(":");
            List<String> entries = entry.getValue();
            for(int i = 0;i < entries.size();i++) {
                String extension = entries.get(i);
                builder.append(extension);
                if (i != (entries.size() - 1)) {
                    builder.append(",");
                }
            }
            builder.append(String.format("%n"));
            writer.println(builder.toString());
        }
        StringBuilder customEntriesBuilder = new StringBuilder();
        for(Map.Entry<String, String> customValue : this.otherValues.entrySet()) {
            customEntriesBuilder.append(customValue.getKey());
            customEntriesBuilder.append(" = ");
            customEntriesBuilder.append(customValue.getValue());
            customEntriesBuilder.append(String.format("%n"));
        }
        writer.println(customEntriesBuilder.toString());
        writer.close();
        ofstream.close();
    }

    /**
     * Returns name of the local cache file.
     * @return name of the local cache file
     */
    @Override
    public String getLocalCacheFilename() {
        return this.localCacheFilename;
    }

    /**
     * Returns sequence of characters that appear in the beginning of tag.
     * @return sequence of characters that appear in the beginning of tag.
     */
    @Override
    public String getTagStartSequence() {
        return this.tagStartSequence;
    }

    /**
     * Returns current version of the tool.
     * @return current version of the tool.
     */
    @Override
    public String getVersion() {
        return (new DefaultConfigProvider().getVersion());
    }

    /**
     * Returns Map containing entries that consist of path-extension list pairs.
     * @return Map containing entries that consist of path-extension list pairs.
     */
    @Override
    public Map<String, ArrayList<String>> getPaths() {
        return this.paths;
    }

    /**
     * Returns other value identified by key stored by config provider object.
     * @param key key by which the value is identified.
     * @return value identified by key, or null if not present.
     */
    @Override
    public String getOtherValue(String key) {
        return this.otherValues.get(key);
    }

    /**
     * Returns map that contains key-value pairs of other configuration variables.
     * @return map that contains key-value pairs of other configuration variables.
     */
    @Override
    public Map<String, String> getOtherValues() {
        return this.otherValues;
    }

    /**
     * Sets internal values of this object to those from other provider (with exception for 'version' value
     * - this value is always hardcoded and taken from DefaultConfigProvider).
     * @param other other provider
     */
    public void getConfigFromOtherProvider(ConfigurationProvider other) {
        this.localCacheFilename = other.getLocalCacheFilename();
        this.tagStartSequence = other.getTagStartSequence();
        this.paths = other.getPaths();
    }

    /**
     * Returns boolean indicating if file was loaded.
     * @return boolean indicating if file was loaded.
     */
    public boolean isFileLoaded() {
        return this.fileLoaded;
    }
}
