package tyre.commons.parser;

import freemarker.template.utility.StringUtil;
import tyre.commons.configuration.ConfigurationManager;
import tyre.commons.requirements.model.*;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main parser for searching tags of requirements in code.
 */
//@req(REQ-87)
public class FileParser {

    //@req(REQ-133)
    public FileParser(){}

    public FileParser(String fileRootPath) {
        this.fileRootPath = fileRootPath;
    }

    private static final Logger LOGGER = Logger.getLogger(FileParser.class.getPackage().getName());

    /**
     * Starting sequence for tag.
     * Starting tag sequence retrieved from configuration (excluding newline at the end).
     */
    public static final String TAG_START = ConfigurationManager.getInstance().getTagStartSequence() +
            "[\\s&&[^\\n]]*";

    /**
     * Regular expression for id given in tag.
     * Sequence of any symbols excluding: *()&^%$#@!"'?<>:
     */
    public static final String TAG_ID = "([^*()&\\^%$#@!\"'?<>:]+)";

    /**
     * Regular expression for version given in tag. It's optional.
     * Sequence of any symbols excluding: *()&^%$#@!"'?<>
     */
    public static final String TAG_VERSION = "(?::([^*()&\\^%$#@!\"'?<>]+))?";

    /**
     *  Pattern for requirement tag matching concatenated from {@link #TAG_START}, {@link #TAG_ID}, {@link #TAG_VERSION}
     *  with () brackets.
     */
    private static final Pattern TAG_PATTERN = Pattern.compile(TAG_START +"\\(" + TAG_ID + TAG_VERSION + "\\)");

    private Map<String, ArrayList<FileReference>> references;
    private String filename;
    private int lineNumber;
    //@req(REQ-133)
    private String fileRootPath;

    //@req(REQ-122)
    public static final String NO_VERSION = "$NO_VERSION";

    /**
     * Parses given line and searches for proper tag defined by {@link #TAG_PATTERN} and adds them to {@link #references} HashMap.
     * Keys are assigned as "TAG_ID:VERSION".
     * If the tag has no given version, default "1" is used.
     * @param line Parsed line's content.
     */
    private void parseLine(String line) {
        // prepare the matcher
        Matcher matcher = TAG_PATTERN.matcher(line);

        // default version of requirement
        String version = NO_VERSION;

        while(matcher.find()) {
            String idGroup = matcher.group(1);
            String versionGroup = matcher.group(2);

            // if version is provided, we change it to that value
            if(versionGroup != null) {
                version = versionGroup;
            }

            // if the id is provided
            if(idGroup != null) {
                String key = idGroup + ":" + version;

                // if the key doesn't exist in HashMap yet
                if(!references.containsKey(key)) {
                    references.put(key, new ArrayList<FileReference>());
                }

                // add new element to references array for the given key
                //@req(REQ-133)
                FileReference newFileReference = new FileReference(filename, lineNumber);
                if(this.fileRootPath != null && !this.fileRootPath.isEmpty()){
                    String relativePath = newFileReference.getFilePath().replaceFirst(this.fileRootPath.replace("\\","\\\\"), "");
                    newFileReference.setRelativePath(relativePath);
                }
                references.get(key).add(newFileReference);
            }
        }
    }

    /**
     * Handle reading of file line by line.
     * @param reader The reader with loaded stream reader.
     * @throws IOException
     */
    private void handleReader(BufferedReader reader) throws IOException {
        String line;
        lineNumber = 1;

        // iterate over all lines from reader
        while((line = reader.readLine()) != null) {
            parseLine(line);
            lineNumber++;
        }
    }

    /**
     * Parses given file and gets found requirement tags.
     * @param filename Path to the checked file.
     * @return HashMap containing ArrayList of {@link tyre.commons.requirements.model.FileReference} of parsed file or null if there was an error.
     */
    public Map<String, ArrayList<FileReference>> parse(String filename) {
        this.references = new LinkedHashMap<String, ArrayList<FileReference>>();
        this.filename = filename;

        FileInputStream fstream;

        // create file input stream
        try {
            fstream = new FileInputStream(filename);
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Parsed file not found!", e);
            return null;
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(fstream));

        // handle the file
        try {
            this.handleReader(reader);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "IO error when parsing file!", e);
            return null;
        }

        // return references found
        return references;
    }
}