package tyre.commons.parser;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class use to get files with given extensions
 * Usage:
 * Call constructor with path to root directory and list of extensions,
 * then call one of class methods
 */
//@req(REQ-62)
public class DirectoryScanner {

    private File rootDir;
    private String[] extensions;

    public DirectoryScanner(String rootPath, List<String> extensions) {
        this.rootDir = new File(rootPath);
        if (extensions == null) {
            this.extensions = null;
        } else {
            this.extensions = extensions.toArray(new String[extensions.size()]);
        }
    }

    /**
     * Method to retrieve all files from given directory and subdirectories with given extensions
     *
     * @return Set of files names from given directory and subdirectories
     */
    public Set<String> getAllFilesWithExtensions() {
        Collection<File> files = FileUtils.listFiles(this.rootDir, this.extensions, true);
        Set<String> filesSet = new HashSet<String>(files.size());
        for (File file : files) {
            filesSet.add(file.getPath());
        }
        return filesSet;
    }

    /**
     * Method to retrieve all files from directory with given extensions
     *
     * @return Set of files names from given directory
     */
    public Set<String> getFilesWithExtensions() {
        Collection<File> files = FileUtils.listFiles(this.rootDir, this.extensions, false);
        Set<String> filesSet = new HashSet<String>(files.size());
        for (File file : files) {
            filesSet.add(file.getPath());
        }
        return filesSet;
    }
}
