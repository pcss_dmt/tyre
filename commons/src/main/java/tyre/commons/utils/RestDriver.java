package tyre.commons.utils;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import tyre.commons.requirements.management.ProviderInternalErrorException;
import tyre.commons.ui.LoginCredentials;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class using to manage communication with external api
 */
public class RestDriver {
    private static final Logger LOGGER = Logger.getLogger(RestDriver.class.getPackage().getName());
    private static final String exceptionMessage = "Unable to retrieve data from provider (driver error)";
    private final URI uri;
    private final Client client;

    public RestDriver(final String uri, final LoginCredentials credentials) throws ProviderInternalErrorException {
        if (uri == null)
            throw new ProviderInternalErrorException(exceptionMessage);

        this.uri = UriBuilder.fromUri(uri).build();
        this.client = ClientBuilder.newClient();

        if (credentials != null)
            this.client.register(HttpAuthenticationFeature.basic(credentials.getUsername(), credentials.getPassword()));
    }

    /**
     * Method return url from concatenate paths from constructor
     *
     * @return concatenate paths from constructor
     */
    public URI getUri() {
        return this.uri;
    }

    /**
     * Method converts response returned by sendRequest to String
     *
     * @return string with server response
     */
    public String getResponse(final URI uri) throws ProviderInternalErrorException {
        if (uri == null)
            throw new ProviderInternalErrorException("Wrong URI");
        return this.sendRequest(uri).readEntity(String.class);
    }

    private Invocation.Builder buildRequest(final Client client, final URI uri) {
        return client.target(uri).request(MediaType.APPLICATION_JSON_TYPE);
    }

    private Response sendRequest(final URI uri) throws ProviderInternalErrorException {
        Response response = null;
        try {
            response = this.buildRequest(this.client, uri).get();
            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                LOGGER.log(Level.SEVERE, "HTTP error response", response.getStatus());
                throw new ProviderInternalErrorException(exceptionMessage);
            }
        } catch (ProcessingException e) {
            LOGGER.log(Level.SEVERE, "Runtime processing failure during HTTP request or response processing", e);
            throw new ProviderInternalErrorException(exceptionMessage, e);
        } catch (WebApplicationException e) {
            LOGGER.log(Level.SEVERE, "Runtime exception for applications (HTTP error response)", e);
            throw new ProviderInternalErrorException(exceptionMessage, e);
        } catch (IllegalArgumentException e) {
            LOGGER.log(Level.SEVERE, "Method has been passed an illegal or inappropriate argument", e);
            throw new ProviderInternalErrorException(exceptionMessage, e);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "General exception raised", e);
            throw new ProviderInternalErrorException(exceptionMessage, e);
        }
        return response;
    }
}