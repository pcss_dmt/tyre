package tyre.commons.utils.merger;

import tyre.commons.parser.FileParser;
import tyre.commons.requirements.management.RequirementManager;
import tyre.commons.requirements.model.ParserEntry;
import tyre.commons.requirements.model.Requirement;
import tyre.commons.ui.UIMessagePrompt;

import java.util.List;

/**
 * Class that provides merging {@link tyre.commons.requirements.model.ParserEntry}
 * with requirements retrieved by {@link tyre.commons.requirements.management.RequirementManager}.
 */
//req(REQ-121)
public class RequirementMerger {

    /**
     * Requirement manager that manages local storage.
     */
    private RequirementManager manager;

    private UIMessagePrompt ui;

    /**
     * Fetches requirement from requirement manager.
     * If no version was provided, shows {@link UIMessagePrompt#print(String)} message about number of versions available for that requirement.
     * @param entry Entry retrieved from parser.
     * @return Requirement of provided id and version or null if requirement was not found.
     */
    public Requirement fetchRequirement(ParserEntry entry) {
        List<Requirement> requirementsList = manager.getById(entry.getId());

        // tell the user that there are multiple versions available if the parsed tag was without version.
        if(entry.getVersion().equals(FileParser.NO_VERSION) && !requirementsList.isEmpty()) {
            ui.print("WARNING! Requirement " + entry.getId() + " was provided with no version!\nThere "
                    + ((requirementsList.size() == 1) ? "is " : "are ") + requirementsList.size()
                    + " possible version" + ((requirementsList.size() > 1) ? "s" : "") + "!\n");
        }

        for (Requirement requirement : requirementsList) {
            if (entry.getVersion().equals(requirement.getVersion())) {
                return requirement;
            }
        }
        return null;
    }

    public RequirementMerger(RequirementManager manager, UIMessagePrompt ui) {
        this.manager = manager;
        this.ui = ui;
    }
}
