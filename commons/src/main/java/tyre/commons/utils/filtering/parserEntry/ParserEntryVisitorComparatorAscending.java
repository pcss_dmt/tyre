package tyre.commons.utils.filtering.parserEntry;

import tyre.commons.requirements.model.ParserEntry;
import tyre.commons.utils.filtering.Visitor;

import java.util.Comparator;

/**
 * Public comparator class to help sort requirements in ascending order using visitors to get data.
 */
public class ParserEntryVisitorComparatorAscending implements Comparator<ParserEntry> {

    private Visitor<ParserEntry, String> visitor;

    public ParserEntryVisitorComparatorAscending(Visitor<ParserEntry, String> visitor) {
        this.visitor = visitor;
    }

    public int compare(ParserEntry t0, ParserEntry t1) {
        return this.visitor.visit(t0).compareTo(this.visitor.visit(t1));
    }
}
