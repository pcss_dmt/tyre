package tyre.commons.utils.filtering.requirement;

import tyre.commons.requirements.model.Requirement;
import tyre.commons.utils.filtering.Visitor;

import java.util.Comparator;

/**
 * Public comparator class to help sort requirements in ascending order using visitors to get data.
 */
public class RequirementVisitorComparatorAscending implements Comparator<Requirement> {

    private Visitor<Requirement, String> visitor;

    public RequirementVisitorComparatorAscending(Visitor<Requirement, String> visitor) {
        this.visitor = visitor;
    }

    public int compare(Requirement requirement, Requirement t1) {
        return this.visitor.visit(requirement).compareTo(this.visitor.visit(t1));
    }
}