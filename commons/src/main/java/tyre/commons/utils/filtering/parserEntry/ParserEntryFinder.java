package tyre.commons.utils.filtering.parserEntry;

import tyre.commons.configuration.ConfigurationManager;
import tyre.commons.parser.DirectoryScanner;
import tyre.commons.parser.FileParser;
import tyre.commons.requirements.model.FileReference;
import tyre.commons.requirements.model.ParserEntry;
import tyre.commons.utils.filtering.Visitor;

import java.util.*;
import java.util.regex.Pattern;

//@req(REQ-143)
public class ParserEntryFinder {

    /**
     * Static visitor that implements getting the id of parser entry.
     */
    public static final Visitor<ParserEntry, String> idVisitor = new Visitor<ParserEntry, String>() {
        public String visit(ParserEntry entry) {
            return entry.getId();
        }
    };

    /**
     * Static visitor that implements getting the version of parser entry.
     */
    public static final Visitor<ParserEntry, String> versionVisitor = new Visitor<ParserEntry, String>() {
        public String visit(ParserEntry entry) {
            return entry.getVersion();
        }
    };

    /**
     * Type of sorting applied.
     */
    private Comparator<ParserEntry> visitorComparator;

    /**
     * List of applied filters.
     */
    private Map<String, Visitor<ParserEntry, String>> filters;

    /**
     * List of filtered/sorted requirements.
     */
    private List<ParserEntry> list;

    /**
     * Parsing path.
     */
    private String path;

    /**
     * Parsing extensions.
     */
    private List<String> extensions = null;

    public ParserEntryFinder() {
        filters = new HashMap<String, Visitor<ParserEntry, String>>();
        list = null;
        visitorComparator = null;
    }

    /**
     * Merges two maps containing requirement and file references pairs.
     * @param destination map to which result should be aggregated
     * @param source map from which entries are fetched
     */
    private void mergeMaps(Map<String, ArrayList<FileReference>> destination,
                           Map<String, ArrayList<FileReference>> source) {
        for(Map.Entry<String, ArrayList<FileReference>> entry : source.entrySet()) {
            if (destination.containsKey(entry.getKey())) {
                ArrayList<FileReference> list = destination.get(entry.getKey());
                list.addAll(entry.getValue());
            } else {
                destination.put(entry.getKey(),entry.getValue());
            }
        }
    }

    /**
     * Parses set of files as defined by config. If user provided extension list by command parameters, extensions
     * provided by config are overridden.
     */
    private void parseUsingConfig() {
        boolean overrideExtensions = (this.extensions != null);
        Map<String, ArrayList<String>> effectivePaths = ConfigurationManager.getInstance().getPaths();
        Map<String, ArrayList<FileReference>> aggregatedResults = new LinkedHashMap<String, ArrayList<FileReference>>();
        for (Map.Entry<String, ArrayList<String>> path : effectivePaths.entrySet()) {
            DirectoryScanner scanner;
            if (overrideExtensions) {
                scanner = new DirectoryScanner(path.getKey(), this.extensions);
            } else {
                scanner = new DirectoryScanner(path.getKey(), path.getValue());
            }
            Set<String> files = scanner.getAllFilesWithExtensions();
            for (String file : files) {
                Map<String, ArrayList<FileReference>> parserOutput = new FileParser(path.getKey()).parse(file);
                mergeMaps(aggregatedResults, parserOutput);
            }
        }
        list = ParserEntry.getListFromMap(aggregatedResults);
    }

    /**
     * Parses set of files as defined by command parameters.
     */
    private void parseUsingParameters() {
        DirectoryScanner scanner = new DirectoryScanner(this.path, this.extensions);
        Map<String, ArrayList<FileReference>> aggregatedResults = new LinkedHashMap<String, ArrayList<FileReference>>();
        Set<String> files = scanner.getAllFilesWithExtensions();
        for (String file : files) {
            Map<String, ArrayList<FileReference>> parserOutput = new FileParser(this.path).parse(file);
            mergeMaps(aggregatedResults, parserOutput);
        }
        list = ParserEntry.getListFromMap(aggregatedResults);
    }

    /**
     * Chooses parsing mode accordingly to user provided parameters.
     */
    private void parse() {
        if (this.path == null) {
            this.parseUsingConfig();
        } else {
            this.parseUsingParameters();
        }
    }

    /**
     * Filters the list using pattern for a field specified by a given visitor.
     *
     * @param pattern Pattern to filter.
     * @param visitor Visitor class specifying the field to be filtered.
     */
    private List<ParserEntry> filter(String pattern, Visitor<ParserEntry, String> visitor) {
        Pattern filter = Pattern.compile(pattern);

        List<ParserEntry> tmp = new ArrayList<ParserEntry>();

        for (ParserEntry req : list) {
            if (filter.matcher(visitor.visit(req)).matches()) {
                tmp.add(req);
            }
        }

        return new ArrayList<ParserEntry>(tmp);
    }

    /**
     * Method for sorting ParserEntry list using given comparator.
     */
    private void sort() {
        // sort using given comparator
        if (!list.isEmpty()) {
            Collections.sort(list, visitorComparator);
        }
    }

    /**
     * Applies filtering operations that were described by command parameters.
     */
    private void applyFilters() {
        for (Map.Entry<String, Visitor<ParserEntry, String>> filter : this.filters.entrySet()) {
            list = filter(filter.getKey(), filter.getValue());
        }
    }

    /**
     * Applies sorting operation that was described by command parameters.
     */
    private void applySorting() {
        if (this.visitorComparator == null) {
            return;
        }
        sort();
    }

    /**
     * Sets internal sorting comparator to ascending mode using given visitor.
     * @param visitor Visitor that returns appropriate field.
     */
    public void setSortAscending(Visitor<ParserEntry, String> visitor) {
        visitorComparator = new ParserEntryVisitorComparatorAscending(visitor);
    }

    /**
     * Sets internal sorting comparator to descending mode using given visitor.
     * @param visitor Visitor that returns appropriate field.
     */
    public void setSortDescending(Visitor<ParserEntry, String> visitor) {
        visitorComparator = new ParserEntryVisitorComparatorDescending(visitor);
    }

    /**
     * Adds filter.
     * @param value Value that will be filtered.
     * @param visitor Visitor that returns proper field for filtering.
     */
    public void addFilter(String value, Visitor<ParserEntry, String> visitor) {
        filters.put(value, visitor);
    }

    /**
     * Set path in which parser will look for files. If not set, parser will use default configuration.
     * @param path Path to parent directory of files to be parsed.
     */
    public void setPath(String path) {
        this.path = path;
    }

    public List<String> getPaths() {
        List<String> paths = new ArrayList<String>();

        if(this.path != null && this.path.length() > 0) {
            paths.add(this.path);
        } else {
            for(Map.Entry<String, ArrayList<String>> path : ConfigurationManager
                    .getInstance().getPaths().entrySet()) {
                paths.add(path.getKey());
            }
        }
        return paths;
    }

    /**
     * Extensions of files that will be parsed.
     * @param extensions List of possible extensions (without a dot in the beginning).
     */
    public void setExtensions(List<String> extensions) {
        this.extensions = extensions;
    }

    public List<String> getExtensions() {
        if(this.extensions != null && !this.extensions.isEmpty()) {
            return this.extensions;
        } else {
            Set<String> extensions = new HashSet<String>();
            for(Map.Entry<String, ArrayList<String>> paths : ConfigurationManager
                    .getInstance().getPaths().entrySet()) {
                for(String ext : paths.getValue()) {
                    extensions.add(ext);
                }
            }

            return new ArrayList<String>(extensions);
        }
    }

    /**
     * Applies added filters and sorting to the list provided by parser from configured file path.
     * @return Filtered/sorted list.
     */
    public List<ParserEntry> apply() {
        parse();
        applyFilters();
        applySorting();

        return list;
    }
}
