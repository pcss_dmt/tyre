package tyre.commons.utils.filtering;

/**
 * Public interface to easily access specific fields of a class in a generic and polymorphic way.
 */
public interface Visitor<T, U> {
    U visit(T object);
}
