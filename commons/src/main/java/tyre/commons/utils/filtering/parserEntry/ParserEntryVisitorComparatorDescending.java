package tyre.commons.utils.filtering.parserEntry;

import tyre.commons.requirements.model.ParserEntry;
import tyre.commons.utils.filtering.Visitor;

import java.util.Comparator;

/**
 * Public comparator class to help sort requirements in descending order using visitors to get data.
 */
public class ParserEntryVisitorComparatorDescending implements Comparator<ParserEntry> {

    private Visitor<ParserEntry, String> visitor;

    public ParserEntryVisitorComparatorDescending(Visitor<ParserEntry, String> visitor) {
        this.visitor = visitor;
    }

    public int compare(ParserEntry t0, ParserEntry t1) {
        return this.visitor.visit(t1).compareTo(this.visitor.visit(t0));
    }
}
