package tyre.commons.utils.filtering.requirement;

import tyre.commons.requirements.model.Requirement;
import tyre.commons.utils.filtering.Visitor;

import java.util.*;
import java.util.regex.Pattern;

public class RequirementFinder {

    /**
     * Static visitor that implements getting the id of requirement.
     */
    public static final Visitor<Requirement, String> idVisitor = new Visitor<Requirement, String>() {
        public String visit(Requirement requirement) {
            return requirement.getId();
        }
    };

    /**
     * Static visitor that implements getting the title of requirement.
     */
    public static final Visitor<Requirement, String> titleVisitor = new Visitor<Requirement, String>() {
        public String visit(Requirement requirement) {
            return requirement.getTitle();
        }
    };

    /**
     * Static visitor that implements getting the version of requirement.
     */
    public static final Visitor<Requirement, String> versionVisitor = new Visitor<Requirement, String>() {
        public String visit(Requirement requirement) {
            return requirement.getVersion();
        }
    };

    /**
     * Static visitor that implements getting the location of requirement.
     */
    public static final Visitor<Requirement, String> locationVisitor = new Visitor<Requirement, String>() {
        public String visit(Requirement requirement) {
            return requirement.getLocation();
        }
    };

    /**
     * List of applied filters.
     */
    private Map<String, Visitor<Requirement, String>> filters;

    /**
     * Type of sorting applied.
     */
    private Comparator<Requirement> visitorComparator;

    /**
     * List of filtered/sorted requirements.
     */
    private List<Requirement> list;

    public RequirementFinder() {
        filters = new HashMap<String, Visitor<Requirement, String>>();
        list = new ArrayList<Requirement>();
        visitorComparator = null;
    }

    /**
     * Filters the Requirement list using pattern for a field specified by a given visitor.
     *
     * @param pattern Pattern to filter.
     * @param visitor Visitor class specifying the field to be filtered.
     * @return Filtered list.
     */
    private List<Requirement> filter(String pattern, Visitor<Requirement, String> visitor) {
        Pattern filter = Pattern.compile(pattern);

        List<Requirement> tmp = new ArrayList<Requirement>();

        for (Requirement req : list) {
            if (filter.matcher(visitor.visit(req)).matches()) {
                tmp.add(req);
            }
        }
        return tmp;
    }

    /**
     * Method for sorting Requirement list using given comparator.
     */
    public void sort() {
        // sort using given comparator
        if (!list.isEmpty()) {
            Collections.sort(list, visitorComparator);
        }
    }

    /**
     * Applies filtering operations that were described by command parameters.
     */
    private void applyFilters() {
        for (Map.Entry<String, Visitor<Requirement, String>> filter : this.filters.entrySet()) {
            this.list = filter(filter.getKey(), filter.getValue());
        }
    }

    /**
     * Applies sorting operation that was described by command parameters.
     */
    private void applySorting() {
        if (this.visitorComparator == null) {
            return;
        }
        sort();
    }

    /**
     * Sets internal sorting comparator to ascending mode using given visitor.
     * @param visitor Visitor that returns appropriate field.
     */
    public void setSortAscending(Visitor<Requirement, String> visitor) {
        visitorComparator = new RequirementVisitorComparatorAscending(visitor);
    }

    /**
     * Sets internal sorting comparator to descending mode using given visitor.
     * @param visitor Visitor that returns appropriate field.
     */
    public void setSortDescending(Visitor<Requirement, String> visitor) {
        visitorComparator = new RequirementVisitorComparatorDescending(visitor);
    }

    /**
     * Adds filter.
     * @param value Value that will be filtered.
     * @param visitor Visitor that returns proper field for filtering.
     */
    public void addFilter(String value, Visitor<Requirement, String> visitor) {
        filters.put(value, visitor);
    }

    /**
     * Sets the list of requirements to be filtered and/or sorted.
     * @param requirements List of requirements.
     */
    public void setList(List<Requirement> requirements) {
        list.addAll(requirements);
    }

    /**
     * Applies added filters and sorting to the set list.
     * @return Filtered list.
     */
    public List<Requirement> apply() {
        applyFilters();
        applySorting();

        return list;
    }
}
