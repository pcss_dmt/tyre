package tyre.commons.requirements.management;

import tyre.commons.requirements.model.Requirement;

import java.util.List;

/**
 * Interface that provides data fetching operations for different requirement sources.
 */
public interface RequirementProvider {
    /**
     * Returns all requirements in this requirements source.
     *
     * @return all requirements stored in this requirements source.
     */
    List<Requirement> getAllRequirements();

    /**
     * Gets list containing references to all requirements in this requirements source that are identified by
     * provided id.
     *
     * @param id id that desired requirement will be identified by.
     * @return list containing references to all requirements in this requirements source that are identified by
     * provided id.
     */
    List<Requirement> getById(String id);

    /**
     * Returns string that will identify this requirements source type.
     *
     * @return string that will identify this requirements source type.
     */
    String getProviderType();
}