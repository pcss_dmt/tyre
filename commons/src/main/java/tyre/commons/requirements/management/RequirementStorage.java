package tyre.commons.requirements.management;

import tyre.commons.configuration.ConfigurationManager;
import tyre.commons.requirements.model.Requirement;
import tyre.commons.requirements.model.StorageEntry;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Requirements storage that stores latest requirement versions.
 */
//@req(REQ-127)
public class RequirementStorage implements Serializable {

    private List<StorageEntry> requirements;

    private String filename;

    private static final Logger logger = Logger.getLogger(RequirementStorage.class.
            getPackage().getName());

    public RequirementStorage() {
        requirements = new ArrayList<StorageEntry>();
        filename = ConfigurationManager.getInstance().getLocalCacheFilename();
        load();
    }

    /**
     * Gets all requirements stored in this object.
     *
     * @return all requirements stored in this object
     */
    public List<Requirement> getAllRequirements() {
        List<Requirement> output = new ArrayList<Requirement>();
        for (StorageEntry entry : this.requirements) {
            Requirement requirement = entry.getRequirement();
            output.add(requirement);
        }
        return output;
    }

    /**
     * Gets requirements identified by provided id.
     *
     * @param id id that desired requirement will be identified by.
     * @return List of requirements identified by this id
     */
    public List<Requirement> getById(String id) {
        List<Requirement> output = new ArrayList<Requirement>();
        for (StorageEntry entry : this.requirements) {
            Requirement requirement = entry.getRequirement();
            if (requirement.getId().equals(id)) {
                output.add(requirement);
            }
        }
        return output;
    }

    /**
     * Updates requirement in this object.
     *
     * @param requirement the requirement that we want to update
     * @param providerId string that identifies requirement origin
     * @return boolean indicating requirement was found and updated.
     */
    public boolean updateRequirement(Requirement requirement, final String providerId) {
        for (StorageEntry entry : requirements) {
            if (entry.canBeUpdated(requirement)) {
                entry.updateRequirement(requirement, providerId);
                save();
                return true;
            }
        }
        return false;
    }

    /**
     * Tries to update requirement if it exists, if not, the requirement is added.
     * @param requirement requirement that will be updated
     * @param providerId string that identifies provider
     * @return boolean indicating if operation was successful.
     */
    public boolean addOrUpdate(Requirement requirement, final String providerId) {
        boolean success = updateRequirement(requirement, providerId);
        if (!success) {
            success = requirements.add(new StorageEntry(requirement, providerId));
            save();
        }
        return success;
    }

    /**
     * Clears requirement storage.
     */
    public void clear() {
        requirements.clear();
    }

    /**
     * Loads this storage from file.
     *
     * @return boolean indicating if deserialization was completed successfully
     */
    public boolean load() {
        FileInputStream fileStream = null;
        ObjectInputStream objectStream = null;
        try {
            fileStream = new FileInputStream(filename);
        } catch (FileNotFoundException e) {
            logger.log(Level.WARNING, "Cannot open file: \'" + filename + "\' - error: " + e.getMessage());
            return false;
        }
        try {
            objectStream = new ObjectInputStream(fileStream);
        } catch (IOException e) {
            logger.log(Level.WARNING, "IO error: " + e.getMessage());
            return false;
        }
        try {
            requirements = ((RequirementStorage) objectStream.readObject()).requirements;
        } catch (ClassNotFoundException e) {
            logger.log(Level.WARNING, "File cannot be deserialized to RequirementStorage object: " + e.getMessage());
            return false;
        } catch (IOException e) {
            logger.log(Level.WARNING, "IO error: " + e.getMessage());
            return false;
        }
        try {
            objectStream.close();
            fileStream.close();
        } catch (IOException e) {
            logger.log(Level.WARNING, "Cannot close streams - error: " + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Saves this storage to file.
     *
     * @return boolean indicating if save was completed successfully.
     */
    public boolean save() {
        FileOutputStream fileStream = null;
        ObjectOutputStream objectStream = null;
        boolean flag = false;
        try {
            fileStream = new FileOutputStream(this.filename);
            objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(this);
            objectStream.close();
            fileStream.close();
            flag = true;
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE,
                    "File for serializing requirement storage could not be found!", e);
        } catch (IOException e) {
            logger.log(Level.SEVERE,
                    "Error occurred during serialization of requirement storage!", e);
        }
        return flag;
    }
}
