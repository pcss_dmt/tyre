package tyre.commons.requirements.management;

public class ProviderInternalErrorException extends Exception {

    public ProviderInternalErrorException(String message) {
        super(message);
    }

    public ProviderInternalErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProviderInternalErrorException(Throwable cause) {
        super(cause);
    }
}