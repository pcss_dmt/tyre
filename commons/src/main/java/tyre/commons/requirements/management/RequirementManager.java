package tyre.commons.requirements.management;

import tyre.commons.requirements.model.Requirement;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main requirement manager class that keeps requirement storage up to date.
 */
public class RequirementManager {
    private static final Logger logger = Logger.getLogger(RequirementManager.class.
            getPackage().getName());

    private List<RequirementProvider> providers;

    private RequirementStorage requirementStorage;

    static private class ProviderIdRequirementPair {
        private String providerId;
        private Requirement requirement;

        public ProviderIdRequirementPair(String providerId, Requirement requirement) {
            this.providerId = providerId;
            this.requirement = requirement;
        }

        public String getProviderId() {
            return providerId;
        }

        public Requirement getRequirement() {
            return requirement;
        }

    }

    public RequirementManager() {
        providers = new ArrayList<RequirementProvider>();

        requirementStorage = new RequirementStorage();
    }

    /**
     * If given provider is not null nor present in manager it is added to the manager.
     *
     * @param provider given provider.
     * @return boolean indicating if provider was added to the manager
     */
    public boolean addProvider(RequirementProvider provider) {
        if (provider == null || providers.contains(provider)) {
            return false;
        }

        return providers.add(provider);
    }


    /**
     * Gets all requirements from providers.
     *
     * @return all requirements from providers
     */
    public List<Requirement> getAllRequirements() {
        synchronize();
        return requirementStorage.getAllRequirements();
    }

    /**
     * Gets requirements from local cache identified by provided id.
     *
     * @param id id that desired requirement will be identified by.
     * @return List of requirements identified by this id
     */
    public List<Requirement> getById(String id) {
        synchronize(id);
        return requirementStorage.getById(id);
    }

    /**
     * Returns copy of the list that contains references to providers assigned to this manager.
     *
     * @return copy of the list that contains references to providers assigned to this manager.
     */
    public List<RequirementProvider> getProviders() {
        return new ArrayList<RequirementProvider>(providers);
    }

    /**
     * Tries to fetch all requirements from provider - if no ProviderInternalErrorException is raised, all requirements
     * are placed in buffer.
     *
     * @param provider provider from which requirements will be fetched
     * @param buffer   buffer to which requirements will be appended to
     * @return boolean indicating if ProviderInternalExceptionError was raised.
     */
    private static boolean getAllRequirementsFromProvider(
            RequirementProvider provider,
            List<ProviderIdRequirementPair> buffer) {
        if (provider == null || buffer == null) {
            return true;
        }

        List<Requirement> providerRequirements = null;

        try {
            providerRequirements = provider.getAllRequirements();
            for (Requirement requirement : providerRequirements) {
                buffer.add(new ProviderIdRequirementPair(provider.getProviderType(), requirement));
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, "Provider /'" +
                    provider.getProviderType() +
                    "/' reports internal error: " + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Tries to fetch requirement that has desired id from provider - if no ProviderInternalErrorException is raised, all
     * requirement versions from provider are placed in buffer.
     *
     * @param provider provider from which requirement will be fetched
     * @param buffer   buffer to which requirements will be appended to
     * @param id       id of the desired requirement
     * @return boolean indicating if ProviderInternalExceptionError was raised.
     */
    private static boolean getRequirementFromProvider(
            RequirementProvider provider,
            List<ProviderIdRequirementPair> buffer,
            final String id) {
        if (provider == null || buffer == null || id == null) {
            return true;
        }

        List<Requirement> providerRequirements = null;

        try {
            providerRequirements = provider.getById(id);
            for (Requirement requirement : providerRequirements) {
                buffer.add(new ProviderIdRequirementPair(provider.getProviderType(), requirement));
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, "Provider /'" +
                    provider.getProviderType() +
                    "/' reports internal error: " + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Fills requirement storage with fresh data regarding requirements from providers.
     */
    private void synchronize() {
        List<ProviderIdRequirementPair> buffer = new ArrayList<ProviderIdRequirementPair>();
        for (RequirementProvider provider : providers) {
            getAllRequirementsFromProvider(provider, buffer);
        }

        for (ProviderIdRequirementPair pair : buffer) {
            requirementStorage.addOrUpdate(pair.getRequirement(), pair.getProviderId());
        }
    }

    /**
     * Fetches latest data regarding requirement that has given id from providers.
     *
     * @param id id of requirement that we want to have latest data about.
     */
    private void synchronize(String id) {
        List<ProviderIdRequirementPair> buffer = new ArrayList<ProviderIdRequirementPair>();
        for (RequirementProvider provider : providers) {
            getRequirementFromProvider(provider, buffer, id);
        }

        for (ProviderIdRequirementPair pair : buffer) {
            requirementStorage.addOrUpdate(pair.getRequirement(), pair.getProviderId());
        }
    }

    /**
     * Returns requirement storage.
     *
     * @return requirement storage
     */
    public RequirementStorage getRequirementStorage() {
        return requirementStorage;
    }
}
