package tyre.commons.requirements.model;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Class implementing model of references in file (filename and line number)
 */
public class FileReference {
    private String filePath;
    private int lineNumber;
    private String fileName;
    //@req(REQ-133)
    private String relativePath;

    public FileReference(String filePath, int lineNumber) {
        this.filePath = filePath;
        this.lineNumber = lineNumber;
        String[] segments = filePath.split("[/\\\\]");
        this.fileName = segments[segments.length-1];
    }

    /**
     * Returns path of file.
     *
     * @return Path of file.
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Sets path of file.
     *
     * @param filePath Path of file.
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Returns number of line.
     *
     * @return Number of line.
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets number of line.
     *
     * @param lineNumber Number of line.
     */
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRelativePath() { return relativePath; }

    public void setRelativePath(String relativePath) { this.relativePath = relativePath; }

    public String getAbsolutePath() {
        return new File(fileName).getAbsolutePath();
    }

    public String getCanonicalPath() throws IOException {
        return new File(fileName).getCanonicalPath();
    }

    public String getTransformedPath() throws IOException {
        String relative = getRelativePath();

        String transformed = relative.replaceAll("\\\\", "/").replaceAll("\\.\\./","");

        if (transformed.charAt(0) != '/') {
            transformed = "/" + transformed;
        }

        return transformed;
    }
}
