package tyre.commons.requirements.model;

import tyre.commons.validation.ValidationStatus;

import java.util.*;

/**
 * Parser output entry data model class.
 */
public class ParserEntry {
    /**
     * Id of requirement that was tagged in parsed file.
     */
    private String id;

    /**
     * Version of requirement that was tagged in parsed file.
     */
    private String version;

    /**
     * List of files and line numbers where requirement with this id and version is referenced.
     */
    private List<FileReference> references;

    /**
     * Status of validation, or null if validation was not performed
     */
    private ValidationStatus validationStatus;

    private List<String> filesNames;

    /**
     * Translates parser key format into id/version fields.
     * @param key key that will be converted
     */
    private void translateKey(String key) {
        int pos = key.lastIndexOf(':');
        if (pos == -1) {
            return; // invalid output from parser
        }
        this.id = key.substring(0, pos);
        this.version = key.substring(pos + 1, key.length());
    }

    /**
     * Sets objects fields basing on argument values.
     * @param entry map entry that will provide values for this object.
     */
    public void fromEntry(Map.Entry<String, ArrayList<FileReference>> entry) {
        this.translateKey(entry.getKey());
        this.references = entry.getValue();
    }

    /**
     * Constructor that creates object basing on map entry.
     * @param entry map entry that will provide values for this object.
     */
    public ParserEntry(Map.Entry<String, ArrayList<FileReference>> entry) {
        this.fromEntry(entry);
        this.validationStatus = ValidationStatus.UNKNOWN;
    }

    /**
     * Creates a new List of parser entries from HashMap retrieved from {@link tyre.commons.parser.FileParser}.
     * @param map Map retrieved from {@link tyre.commons.parser.FileParser}.
     * @return List of ParserEntries converted from the map.
     */
    public static List<ParserEntry> getListFromMap(Map<String, ArrayList<FileReference>> map) {
        List<ParserEntry> list = new ArrayList<ParserEntry>();

        for (Map.Entry<String, ArrayList<FileReference>> entry : map.entrySet()) {
            list.add(new ParserEntry(entry));
        }

        return list;
    }

    public String getId() {
        return id;
    }

    public String getVersion() {
        return version;
    }

    public String getTitle() {
        return id + ":" + version;
    }

    public List<FileReference> getReferences() {
        return references;
    }

    public List<String> getFilesNames() {
        Set<String> uniqueFileNames = new HashSet<String>();
        for (final FileReference fileReference : this.references) {
            uniqueFileNames.add(fileReference.getFileName());
        }
        this.filesNames = new ArrayList<String>(uniqueFileNames);
        return this.filesNames;
    }

    public ValidationStatus getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(ValidationStatus validationStatus) {
        this.validationStatus = validationStatus;
    }
}
