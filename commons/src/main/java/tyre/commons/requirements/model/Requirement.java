package tyre.commons.requirements.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Model class that represents the requirement in the system. Requirement
 * consists of id, version, title and content.
 */
public class Requirement implements Serializable {
    private String id;
    private String version;
    private String title;
    private String location;

    @JsonCreator
    public Requirement(
            @JsonProperty("id") String id,
            @JsonProperty("version") String version,
            @JsonProperty("title") String title,
            @JsonProperty("location") String location) {
        this.id = id;
        this.version = version;
        this.title = title;
        this.location = location;
    }

    /**
     * Returns id of the requirement
     *
     * @return id of the requirement
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id of the requirement to the provided value
     *
     * @param id new id value
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Returns version of the requirement
     *
     * @return version of the requirement
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets version of the requirement to the provided value
     *
     * @param version new version value
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Returns title of the requirement
     *
     * @return title of the requirement
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title of the requirement to the provided value
     *
     * @param title new title value
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns location of the requirement
     *
     * @return location of the requirement
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets location of the requirement to the provided value
     *
     * @param location new content value
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Overridden equals method that returns true if all string values of the
     * fields of requirement are equal. Returns false if provided object is not
     * instance of Requirement class or is null.
     *
     * @param other object that will be compared to this object
     * @return boolean indicating if objects were equal
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        } else if (other == this) {
            return true;
        } else if (!(other instanceof Requirement)) {
            return false;
        }
        Requirement otherRequirement = (Requirement) other;
        return (otherRequirement.getId().equals(this.getId())) &&
                (otherRequirement.getLocation().equals(this.getLocation())) &&
                (otherRequirement.getTitle().equals(this.getTitle())) &&
                (otherRequirement.getVersion().equals(this.getVersion()));
    }

    /**
     * Overridden hash code calculator for Requirement class.
     *
     * @return Proper hash code.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        return result;
    }

}
