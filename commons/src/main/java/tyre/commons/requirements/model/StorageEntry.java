package tyre.commons.requirements.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Class that represents entry in requirement storage class.
 */
public class StorageEntry implements Serializable {
    private Requirement requirement;
    private Date timeStamp;
    private String origin;

    public StorageEntry(Requirement requirement,final String origin) {
        this.requirement = requirement;
        this.origin = origin;
        this.timeStamp = new Date();
    }

    /**
     * Gets requirement associated with this particular entry.
     * @return requirement associated with this particular entry
     */
    public Requirement getRequirement() {
        return requirement;
    }

    /**
     * Gets timestamp of last requirement modification.
     * @return timestamp of last requirement modification.
     */
    public Date getTimeStamp() {
        return (Date) timeStamp.clone();
    }

    /**
     * Gets string that identifies origin of this requirement.
     * @return string that identifies origin of this requirement.
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Updates this entry with provided requirement and origin string.
     * @param requirement requirement that this object will be updated with
     * @param origin string that identifies origin of the requirement
     */
    public void updateRequirement(Requirement requirement,final String origin) {
        timeStamp = new Date();
        this.origin = origin;
        this.requirement = requirement;
    }

    /**
     * Checks if provided requirement is suitable to replace current requirement
     * @param other provided requirement
     * @return boolean indicating if provided requirement is suitable to replace current requirement
     */
    public boolean canBeUpdated(Requirement other) {
        if (requirement.getId().equals(other.getId()) && requirement.getVersion().equals(other.getVersion())) {
            return true;
        }
        return false;
    }
}
