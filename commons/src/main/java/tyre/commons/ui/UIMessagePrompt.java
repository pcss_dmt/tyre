package tyre.commons.ui;

/**
 * Interface providing universal user interface messages and prompts.
 */
public interface UIMessagePrompt {
    /**
     * Prints message in a proper way for UI.
     * @param message String to be shown.
     */
    void print(String message);

    /**
     * Shows a prompt that has only one option - OK, like classic GUI's OK message box.
     * @param message String to be shown.
     */
    void showOKPrompt(String message);

    /**
     * Shows and handles the yes/no prompt.
     * @param message Message accompanying prompt.
     * @return The value provided by user. True if yes was chosen, false otherwise.
     */
    boolean showYesNoPrompt(String message);

    /**
     * Shows and handles the login prompt.
     * @param message Message accompanying prompt.
     * @return LoginCredentials with data provided by user.
     */
    LoginCredentials showLoginPrompt(String message);
}
