package tyre.commons.validation;

// @req(REQ-106)
public enum ValidationStatus {
    UNKNOWN, NOT_IMPLEMENTED, PARTIALLY_IMPLEMENTED, FULLY_IMPLEMENTED
}
