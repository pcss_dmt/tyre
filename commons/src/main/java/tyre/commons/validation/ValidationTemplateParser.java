package tyre.commons.validation;

import tyre.commons.parser.FileParser;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// @req(REQ-107)
public class ValidationTemplateParser {
    private static final Logger LOGGER = Logger.getLogger(ValidationTemplateParser.class.getPackage().getName());

    /**
     * Regular expression for given id.
     * Sequence of any symbols excluding: *()&^%$#@!"'?<>:
     */
    private static final String ID_PATTERN = "([^\\s*()&\\^%$#@!\"'?<>:]+)";

    /**
     * Regular expression for given version. It's optional.
     * Sequence of any symbols excluding: *()&^%$#@!"'?<>
     */
    private static final String VERSION_PATTERN = "(?::([^\\s*()&\\^%$#@!\"'?<>]+))?";

    /**
     * Regular expression for requirement definition in validation template.
     * ID[:VERSION] must be at the beginning of a line, with no other characters.
     */
    private static final String REQUIREMENT_HEADER = "^" + ID_PATTERN + VERSION_PATTERN + "\\n*";
    private static final Pattern REQUIREMENT_HEADER_PATTERN = Pattern.compile(REQUIREMENT_HEADER);

    /**
     * Pattern for checking each filename.
     * It has to start with a whitespace other than newline and consist of any sign that is not a newline.
     */
    private static final Pattern FILENAME_PATTERN = Pattern.compile("[\\s&&[^\\n]]([^\\n]+)\\n*");

    private Map<String, ArrayList<String>> validations;
    private String actuallyParsedRequirement = null;
    private String actuallyParsedVersion = null;

    /**
     * Parse one line of input.
     * @param line Line received from BufferedReader.
     */
    private void parseLine(String line) {
        Matcher headerMatcher = REQUIREMENT_HEADER_PATTERN.matcher(line);

        // check if current line is a header defining ID to be checked
        if(headerMatcher.find()) {
            actuallyParsedRequirement = headerMatcher.group(1);
            if(headerMatcher.group(2) != null) {
                actuallyParsedVersion = headerMatcher.group(2);
            } else {
                actuallyParsedVersion = FileParser.NO_VERSION;
            }
            // ...or actual filename
        } else if(actuallyParsedRequirement != null) {
            Matcher filenameMatcher = FILENAME_PATTERN.matcher(line);

            if(filenameMatcher.find()) {
                String key = actuallyParsedRequirement + ":" + actuallyParsedVersion;

                // if the key doesn't exist in HashMap yet
                if(!validations.containsKey(key)) {
                    validations.put(key, new ArrayList<String>());
                }

                if(filenameMatcher.group(1) != null) {
                    validations.get(key).add(filenameMatcher.group(1));
                }
            }
        }
    }

    /**
     * Handle splitting of input stream.
     * @param reader Reader containing input stream.
     * @throws IOException
     */
    private void handleReader(BufferedReader reader) throws IOException {
        String line;

        while((line = reader.readLine()) != null) {
            parseLine(line);
        }
    }

    /**
     * Parses given file into a map of filenames that have to be validated in ValidationProcess.
     * @param validationTemplateFilename Filename of validation template.
     * @return Map with key of ID:VERSION and value of list of filenames in the template.
     */
    public Map<String, ArrayList<String>> parse(String validationTemplateFilename) {
        FileInputStream fstream;

        // create file input stream
        try {
            fstream = new FileInputStream(validationTemplateFilename);
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Parsed validation file not found!", e);
            return null;
        }

        validations = new HashMap<String, ArrayList<String>>();
        actuallyParsedRequirement = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(fstream));

        // handle the file
        try {
            this.handleReader(reader);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "IO error when parsing file!", e);
            return null;
        }

        return validations;
    }
}