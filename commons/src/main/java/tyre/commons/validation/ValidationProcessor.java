package tyre.commons.validation;

import tyre.commons.configuration.ConfigurationManager;
import tyre.commons.requirements.model.ParserEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// @tyre(REQ-106)
public class ValidationProcessor {
    private List<ParserEntry> parserEntryList;
    private ValidationTemplateParser parser;
    private Map<String, ArrayList<String>> referencesMap;

    private static final Pattern IS_REGEX_PATTERN = Pattern.compile("^(?i:regexp|regex):(.*)");

    public ValidationProcessor(final List<ParserEntry> parserEntries, ValidationTemplateParser parser) {
        this.parserEntryList = parserEntries;
        this.parser = parser;
    }

    /**
     * Processes the list of ParserEntries and validates them.
     */
    public void process() {
        String validationTemplate = ConfigurationManager.getInstance().getOtherValue("validation_template");
        if(validationTemplate != null) {
            this.referencesMap = parser.parse(validationTemplate);
            for (final ParserEntry entry : this.parserEntryList) {
                List<String> validationNames = this.referencesMap.get(entry.getTitle());
                if (validationNames != null) {
                    // it counts each occurrence, whether it's regex or plain filename
                    int correctCount = 0;
                    List<String> entryFilenames = entry.getFilesNames();

                    for (final String validationFilename : validationNames) {
                        Matcher isRegex = IS_REGEX_PATTERN.matcher(validationFilename);

                        // if validation template for the file contains a regular expression...
                        if(isRegex.find()) {
                            Pattern regex = Pattern.compile(isRegex.group(1));
                            for(final String filename : entryFilenames) {
                                if(regex.matcher(filename).matches()) {
                                    correctCount++;

                                    break;
                                }
                            }
                        } else {
                            if (entryFilenames.contains(validationFilename)) {
                                correctCount++;
                            }
                        }
                    }

                    // set proper status in an ugly way...
                    if(correctCount == 0) {
                        entry.setValidationStatus(ValidationStatus.NOT_IMPLEMENTED);
                    } else if(correctCount < entryFilenames.size()) {
                        entry.setValidationStatus(ValidationStatus.PARTIALLY_IMPLEMENTED);
                    } else {
                        entry.setValidationStatus(ValidationStatus.FULLY_IMPLEMENTED);
                    }
                } else {
                    entry.setValidationStatus(ValidationStatus.UNKNOWN);
                }
            }
        }
    }
}
