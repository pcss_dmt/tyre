package tyre.commons.report;


import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import tyre.commons.configuration.ConfigurationManager;
import tyre.commons.requirements.management.RequirementManager;
import tyre.commons.requirements.model.ParserEntry;
import tyre.commons.requirements.model.Requirement;
import tyre.commons.ui.LoginCredentials;
import tyre.commons.ui.UIMessagePrompt;
import tyre.commons.utils.filtering.parserEntry.ParserEntryFinder;
import tyre.commons.utils.merger.RequirementMerger;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that generates HTML report basing on provided ParserEntryFinder and default *.ftl template located in
 * ../res/report_templates/report.ftl
 */
// @req(REQ-60)
public class HTMLReporter {

    /**
     * Default output encoding.
     */
    private static final String DEFAULT_ENCODING = "UTF-8";

    /**
     * Default templates directory.
     */
    private static final String DEFAULT_TEMPLATE_PATH = "../res/report_templates";

    /**
     * Default output filename.
     */
    private static final String DEFAULT_OUTPUT_FILENAME = "report.html";

    /**
     * Freemarker configuration.
     */
    private static Configuration configuration = null;

    /**
     * Logger for instances of this class.
     */
    private static final Logger LOGGER = Logger.getLogger(HTMLReporter.class.getPackage().getName());

    /**
     * Reference to requirement manager.
     */
    private RequirementManager requirementManager;

    /**
     * Pair consisting of requirement and parser entry. It is used to aggregate results from parsing and search in the
     */
    public class RequirementParserEntryPair {
        private Requirement requirement;
        private ParserEntry entry;

        private RequirementParserEntryPair(Requirement requirement, ParserEntry entry) {
            this.requirement = requirement;
            this.entry = entry;
        }

        public Requirement getRequirement() {
            return requirement;
        }

        public ParserEntry getEntry() {
            return entry;
        }
    }

    /**
     * Object that implements UIMessagePrompt and gathers all output from RequirementMerger
     */
    private class WarningAggregator implements UIMessagePrompt {
        private List<String> warnings;

        public WarningAggregator() {
            warnings = new ArrayList<String>();
        }

        @Override
        public void print(String message) {
            warnings.add(message);
        }

        @Override
        public void showOKPrompt(String message) {

        }

        @Override
        public boolean showYesNoPrompt(String message) {
            return false;
        }

        @Override
        public LoginCredentials showLoginPrompt(String message) {
            return null;
        }

        public List<String> getWarnings() {
            return warnings;
        }
    }

    public HTMLReporter(RequirementManager requirementManager) {
        if (configuration == null) {
            initFreeMarkerConfiguration();
        }
        this.requirementManager = requirementManager;
    }

    public HTMLReporter(RequirementManager requirementManager,
                        List<ParserEntry> parserEntryList,
                        List<String> paths,
                        List<String> extensions,
                        boolean includeValidation) {
        this(requirementManager);
        generate(parserEntryList, paths, extensions, includeValidation);
    }

    /**
     * Method that creates data model which will be later used in template.
     * @param output output from ParserEntryFinder object
     * @param paths paths that were parsed
     * @param extensions extensions that were parsed
     * @param includeValidation boolean indicating if validation was performed
     * @return data model for the report
     */
    private Map<String, Object> getDataModel(List<ParserEntry> output,
                                             List<String> paths,
                                             List<String> extensions,
                                             boolean includeValidation) {
        Map<String, Object> root = new HashMap<String, Object>();
        List<RequirementParserEntryPair> pairs = new ArrayList<RequirementParserEntryPair>();
        WarningAggregator aggregator = new WarningAggregator();
        List<ParserEntry> sortedOutput = new ArrayList<ParserEntry>(output);

        // @req(REQ-145)
        // sort requirements in the way that will aggregate name and the version
        Collections.sort(sortedOutput, new Comparator<ParserEntry>() {
                    @Override
                    public int compare(ParserEntry parserEntry, ParserEntry t1) {
                        // if requirements have different ids sort them by ids
                        if (!parserEntry.getId().equals(t1.getId())) {
                            return parserEntry.getId().compareTo(t1.getId());
                        }
                        // if not sort them by versions
                        return parserEntry.getVersion().compareTo(t1.getVersion());
                    }
                }
        );

        // @req(REQ-130)
        for (ParserEntry entry : sortedOutput) {
            RequirementMerger merger = new RequirementMerger(requirementManager, aggregator);
            Requirement requirement = merger.fetchRequirement(entry);
            pairs.add(new RequirementParserEntryPair(requirement, entry));
        }

        // @req(REQ-133)
        root.put("repositoryUrl", ConfigurationManager.getInstance().getOtherValue("repositoryUrl"));
        root.put("tyreVersion", ConfigurationManager.getInstance().getVersion());
        root.put("storageFile", ConfigurationManager.getInstance().getLocalCacheFilename());
        root.put("results", pairs);
        root.put("warnings", aggregator.getWarnings());

        // @req(REQ-148)
        root.put("paths", paths);
        root.put("extensions", extensions);

        // @req(REQ-131)
        root.put("validation_enabled", includeValidation);

        // @req(REQ-149)
        root.put("current_date", new Date().toString());

        return root;
    }

    /**
     * Method that generates report basing on provided ParserEntryFinder object.
     * @param parserEntryList object that holds list of parser entries
     * @param paths paths that were parsed
     * @param extensions extensions that were parsed
     * @param includeValidation boolean indicating if validation was performed
     */
    public void generate(List<ParserEntry> parserEntryList,
                         List<String> paths,
                         List<String> extensions,
                         boolean includeValidation) {
        Template template = null;
        try {
            template = configuration.getTemplate("report.ftl");
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Report template cannot be fetched because: " + e.getMessage());
        }

        if (template == null) {
            return;
        }

        Map<String, Object> dataModel = getDataModel(parserEntryList, paths, extensions, includeValidation);


        if (dataModel == null) {
            return;
        }

        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(DEFAULT_OUTPUT_FILENAME);
            OutputStreamWriter writer = new OutputStreamWriter(outStream);
            template.process(dataModel, writer);
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.WARNING, "File stream could not be open because: " + e.getMessage());
        } catch (TemplateException e) {
            LOGGER.log(Level.WARNING, "Freemarker reports template exception: " + e.getMessage());
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Freemarker reports could not process template because: " + e.getMessage());
        }

        try {
            outStream.close();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "File stream could not be closed because: " + e.getMessage());
        } catch (NullPointerException e) {
            LOGGER.log(Level.WARNING, "File stream could not be closed because: " + e.getMessage());
        }

    }

    /**
     * Method that initalizes Freemarker configuration - it is run only once.
     */
    private static void initFreeMarkerConfiguration() {
        configuration = new Configuration(Configuration.VERSION_2_3_23);

        String effectiveEncoding = ConfigurationManager.getInstance().getOtherValue("ReportEncoding");
        if (effectiveEncoding == null) {
            effectiveEncoding = DEFAULT_ENCODING;
        }

        String effectiveTemplatePath = ConfigurationManager.getInstance().getOtherValue("TemplatePath");
        if (effectiveTemplatePath == null) {
            effectiveTemplatePath = DEFAULT_TEMPLATE_PATH;
        }

        configuration.setDefaultEncoding(effectiveEncoding);
        try {
            configuration.setDirectoryForTemplateLoading(new File(effectiveTemplatePath));
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Directory for reports template cannot be set up because: " + e.getMessage());
        }
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    }

}
